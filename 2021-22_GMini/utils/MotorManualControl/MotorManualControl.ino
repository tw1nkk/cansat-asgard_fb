#include "CSPU_Test.h"
#define DEBUG_SCPU
#include "DebugCSPU.h"
#include "GMiniConfig.h"

/*The program "MotorManualControl" is a program that allows you to manually control a motor using five letters.
The feather card will be used for this programme. It is also necessary to have an "H-bridge" (e.g. type: L293D)

The connections :
- One wire must be connected between the 3V terminal of the feather board and pin 8 of the H-bridge
- The GND of the feather card must be connected to pin 12 of the H-bridge.
- Pin EnableBridge1 of the feather is connected to pin 7 of the H-bridge.
- Pin MotorForward1 of the feather is connected to pin 2 of the H-bridge.
- Pin MotorReverse1 of the feather is connected to pin 1 of the H-bridge.
- 
*/


const int enableBridge1 = ScrewLatchPWM_Pin;     // Defined in GMiniConfig.h
const int MotorForward1 = ScrewLatchForward_Pin; // Defined in GMiniConfig.h
const int MotorReverse1 = ScrewLatchReverse_Pin; // Defined in GMiniConfig.h
const int DelayForwardLong = 1000;
const int DelayForwardShort = 500;
const int DelayBackwardsLong = 1000;
const int DelayBackwardsShort = 500;

void setup() {
  DINIT(115200);
  pinMode(MotorForward1, OUTPUT);
  pinMode(MotorReverse1, OUTPUT);
  pinMode(enableBridge1, OUTPUT);

  Serial << "Manual motor control program" << ENDL;
  Serial << "------------------------------" << ENDL;
  Serial << "Write F to turn the motor forward during " << DelayForwardLong << " milliseconds." << ENDL;
  Serial << "Write f to turn the motor forward during " << DelayForwardShort << " milliseconds." << ENDL;
  Serial << "Write B to turn the motor backwards during " << DelayBackwardsLong << " milliseconds." << ENDL;
  Serial << "Write b to turn the motor backwards during " << DelayBackwardsShort << " milliseconds." << ENDL;
  //Serial << "Write S to stop the motor" << ENDL;
}

void loop() {
  char c = CSPU_Test::keyPressed() ;
  switch (c) {

    case 'F':
      Serial << "Motor turns forward during " << DelayForwardLong << " milliseconds." << ENDL;
      digitalWrite(enableBridge1, HIGH); // Active pont en H
      analogWrite(MotorReverse1, 0);
      analogWrite(MotorForward1, Power);
      delay(DelayForwardLong);
      analogWrite(MotorForward1, 0);
      analogWrite(MotorReverse1, 0);
      break;

    case 'f' :
      Serial << "Motor turns forward during " << DelayForwardShort << " milliseconds." << ENDL;
      digitalWrite(enableBridge1, HIGH); // Active pont en H
      analogWrite(MotorReverse1, 0);
      analogWrite(MotorForward1, Power);
      delay(DelayForwardShort);
      analogWrite(MotorForward1, 0);
      analogWrite(MotorReverse1, 0);
      break;

    case 'B':
      Serial << "Motor turns backwards during " << DelayBackwardsLong << " milliseconds." << ENDL;
      digitalWrite(enableBridge1, HIGH); // Active pont en H
      analogWrite(MotorForward1, 0);
      analogWrite(MotorReverse1, Power);
      delay(DelayBackwardsLong);
      analogWrite(MotorForward1, 0);
      analogWrite(MotorReverse1, 0);
      break;

    case 'b':
      Serial << "Motor turns backwards during " << DelayBackwardsShort << " milliseconds." << ENDL;
      digitalWrite(enableBridge1, HIGH); // Active pont en H
      analogWrite(MotorForward1, 0);
      analogWrite(MotorReverse1, Power);
      delay(DelayBackwardsShort);
      analogWrite(MotorForward1, 0);
      analogWrite(MotorReverse1, 0);
      break;

    case 'S':
      Serial << "Motor stop." << ENDL;
      analogWrite(MotorForward1, 0);
      analogWrite(MotorReverse1, 0);
      digitalWrite(enableBridge1, LOW); // désactvie le pont en H
      break;
  }
}
