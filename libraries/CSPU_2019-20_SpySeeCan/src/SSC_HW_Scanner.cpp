/*
 * SSC_HW_Scanner.cpp
 */

#include "SSC_Config.h"
#include "SSC_HW_Scanner.h"
#include "SdFat.h"

SSC_HW_Scanner::SSC_HW_Scanner(const byte unusedAnalogInPin) :
	HardwareScanner(unusedAnalogInPin)
#ifdef RF_ACTIVATE_API_MODE
    ,xbClientAvailable(false),
	xbClient(GroundXBeeAddressSH,GroundXBeeAddressSL)
#endif
{
	flags.SD_CardReaderAvailable = false;
	flags.BMP_SensorAvailable = false;
	flags.SI4432_SensorsAvailable = 0;
}

void SSC_HW_Scanner::SSC_Init() {
  DPRINTSLN(DBG_INIT, "SSC_HW_Scanner::SSC_Init");
  HardwareScanner::init(I2C_lowestAddress, I2C_highestAddress, RF_SerialPortNumber, 400);
  // Do not configure USB debug pin: it is in use already.

  flags.BMP_SensorAvailable = isI2C_SlaveUpAndRunning( I2C_BMP_SensorAddress);

  HardwareSerial* RF = getRF_SerialObject();
  if (RF) {
    RF->begin(RF_SerialBaudRate);
    while (!RF) ;
    DPRINTS(DBG_INIT, "RF Serial init ok at ");
    DPRINT(DBG_INIT, RF_SerialBaudRate );
    DPRINTSLN(DBG_INIT, " bauds");
#ifdef RF_ACTIVATE_API_MODE
    xbClient.begin(*RF); // Cannot fail
    xbClientAvailable=true;
#endif
  }
  else DPRINTSLN(DBG_DIAGNOSTIC, "*** No RF Serial port");

  if (!isSerialPortAvailable(GPS_SerialPortNumber)) {
	  DPRINTSLN(DBG_DIAGNOSTIC, "*** No GPS Serial port");
  }
  else {
	  DPRINTSLN(DBG_INIT, "GPS Serial available");
  }
}

void SSC_HW_Scanner::checkAllSPI_Devices() {
  DPRINTSLN(DBG_CHECK_SPI_DEVICES, "SSC_HW_Scanner::checkAllSPI_Devices");
  // Check for SD Card reader
  SdFat SD;
  pinMode(SD_CardChipSelect, OUTPUT);
  if (SD.begin(SD_CardChipSelect)) {
    flags.SD_CardReaderAvailable = true;
  } else {
    DPRINTSLN(DBG_CHECK_SPI_DEVICES, "Card failed, or not present");
  }
  // check connexion of DRF4432 modules ? Possible ? configurer les flags
  // Au moins configurer les pins si ce n'est pas la lib qui le fait.
}

byte SSC_HW_Scanner::getLED_PinNbr(LED_Type type) {
  byte result;
  switch (type) {
    case Init:
      result = pin_InitializationLED;
      break;
    case Storage:
      result =  pin_StorageLED;
      break;
    case Transmission:
      result =  pin_TransmissionLED;
      break;
    case Acquisition:
      result =  pin_AcquisitionLED;
      break;
    case Heartbeat:
      result =  pin_HeartbeatLED;
      break;
    case Campaign:
      result =  pin_CampaignLED;
      break;
    case UsingEEPROM:
      result =  pin_UsingEEPROM_LED;
      break;
    default:
      DASSERT(false);
      result =  0;
  }
  return result;
}

void SSC_HW_Scanner::printSPI_Diagnostic(Stream& stream) const {
  if (flags.SD_CardReaderAvailable) {
    stream.print(F("SPI bus: SD Card ok. CS="));
  }
  else {
    stream.print(F("*** SPI bus: no SD Card detected. *** CS="));
  }
  stream.println(SD_CardChipSelect);

  for (int i = 0; i< NumSI4432_Cards ; i++)
  {
	  if (flags.SI4432_SensorsAvailable & (1 << i)) {
		  stream.print(F("SPI bus: DRF4432 Card "));
		  stream.print((char) ('A'+i));
		  stream.print(F(" ok. CS="));
	  }
	  else {
		  stream.print(F("*** SPI bus: no DRF4432 "));
		  stream.print((char)('A'+i));
		  stream.print(F(" Card detected. *** CS="));
	  }
	  stream.println(SI4432_ChipSelect[i]);
  }
  return;
}


void SSC_HW_Scanner::printI2C_Diagnostic(Stream& stream) const {
  HardwareScanner::printI2C_Diagnostic(stream);
  delay(200);
  if (flags.BMP_SensorAvailable) {
    stream << F("Assuming I2C slave at ") << I2C_BMP_SensorAddress << F(" is the BMP sensor") << ENDL;
  }
  else
  {
    stream << F("*** Missing BMP sensor at I2C address ") << I2C_BMP_SensorAddress << ENDL;
  }
}

#ifdef IGNORE_EEPROMS
byte SSC_HW_Scanner::getNumExternalEEPROM() const {
#ifndef NO_EEPROMS_ON_BOARD
  Serial << F("*** EEPROMS ignored for test ***") << ENDL;
#endif
  return 0;
}
#endif

void SSC_HW_Scanner::printFullDiagnostic(Stream& stream) const {
	HardwareScanner::printFullDiagnostic(stream);
	delay(300); // Avoid overflowing the stream buffer, in case
				// it is slow (e.g. a radio transmitter)...
	if (isSerialPortAvailable(RF_SerialPortNumber))  {
		stream << F("RF  assumed on Serial") << RF_SerialPortNumber
				<< F(" (") << RF_SerialBaudRate << F(" bauds)") << ENDL;
		if (RF_SerialPortNumber == 2) {
			stream << F("    TX=10, RX = 11") << ENDL;
		}
#ifdef RF_ACTIVATE_API_MODE
		if (xbClientAvailable) {
			stream << F("Using RF API mode. XBee client initialized.");
		} else {
			stream << F("*** RF API mode requested, but XBee client not initialized.");
		}
		stream << F("Using XBee set '") << RF_XBEE_MODULES_SET << "'" << ENDL;
		stream << F(" Receiver (ground) addr: 0x");
		stream.print(GroundXBeeAddressSH,HEX);
		stream << "-0x";
		stream.println(GroundXBeeAddressSL, HEX);
#else
		stream << F("Using RF TRANSPARENT mode");
#endif
	}

	else stream << F("No RF serial port (") << RF_SerialPortNumber << ")" << ENDL;
	if(isSerialPortAvailable(GPS_SerialPortNumber)) {
		stream << F("GPS assumed on Serial") << GPS_SerialPortNumber << ENDL;
	}
	else stream << F("No GPS serial port (") << GPS_SerialPortNumber << ")" << ENDL;
	delay(300); // Avoid overflowing the stream buffer, in case
				// it is slow (e.g. a radio transmitter...
	stream << F("NTCLE Thermistor input on analog pin ") << ThermistorNTCLE_AnalogInPinNbr << ENDL;
	stream << F("NTCLG Thermistor input on analog pin ") << ThermistorNTCLG_AnalogInPinNbr << ENDL;
	stream << F("VMA   Thermistor input on analog pin ") << ThermistorVMA_AnalogInPinNbr << ENDL;
}




