/*
 * SpySeeCan.h 
 * 
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the library should include a tag @ingroup SpySeeCanCSPU 
 * in the class documentation block.
 */

 /** @defgroup SpySeeCanCSPU SpySeeCanCSPU library 
 *  @brief The library of classes specific to the CanSat 2020 SpySeeCan project.
 *  
 *  The SpySeeCanCSPU library contains all the code for the SpySeeCan project, which is assumed not to be reused accross project
 *  
 *  _Dependencies_\n
 *  This library requires the following other generic libraries (in addition to hardware-specific libraries,
 *  and standard Arduino libraries, which are not listed here):
 *  - DebugCSPU
 *  - TimeCSPU
 *  - elapsedMillis
 *  - cansatAsgardCSPU
 *  
 *  _History_\n
 *  The library was created by the 2019-2020 Cansat team (SpySeeCan) based on the IsatisCSPU and IsaTwo libraries.
 */
