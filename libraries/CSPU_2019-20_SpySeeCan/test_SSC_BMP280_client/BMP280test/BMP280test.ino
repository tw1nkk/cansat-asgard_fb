#include "elapsedMillis.h"
#include "SSC_BMP280_Client.h"

SSC_Record record;
elapsedMillis elapsed;
const float mySeaLevelPressure = 1011.0;
constexpr bool csvFormat = true;

SSC_BMP280_Client bmp;

void setup() {
  // put your setup code here, to run once:
  DINIT(115200);
  Wire.begin();
  if (!bmp.begin(mySeaLevelPressure)) {
    Serial.print("Could not find a valid BMP280 sensor, check wiring!");
    while (1);

  }
}
void loop() {
  // put your main code here, to run repeatedly:
  elapsed = 0;
  bool result = bmp.readData(record);
  if (result == true) {
    if (csvFormat) {
      Serial << millis() << "," << record.temperatureBMP << "," << record.pressure << "," << record.altitude << ENDL;

    } else {
      Serial << millis() << ": temp: " << record.temperatureBMP << "°C, pressure:" << record.pressure << " hPa, " << "altitude:" << record.altitude << " m" << ENDL;
    }
  }
  delay(1000);
}
