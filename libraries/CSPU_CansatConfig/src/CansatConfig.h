/*
   System configuration for the common part of any CanSat Project
   This file is expected to be updated for each project, and to be included
   in a project-specific xxxxConfig.h file, which will add the configuration
   related to the secondary mission.
*/

#pragma once
// Silence warnings in standard arduino files (but this will be required in *each*
// cpp and ino file anyway, otherwise Arduino forcefully adds an include of Arduino.h
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "limits.h"
#include "XBeeTypes.h"

/* This function is required to initialize constants (std::ceil is not constexpr).
 * cf https://stackoverflow.com/questions/31952237/looking-for-a-constexpr-ceil-function/31962570
 */
constexpr int32_t constexpr_ceil(float num)
{
    return (static_cast<float>(static_cast<int32_t>(num)) == num)
        ? static_cast<int32_t>(num)
        : static_cast<int32_t>(num) + ((num > 0) ? 1 : 0);
}

// ********************************************************************
// ***************** Calibration settings to update frequently ********
// ********************************************************************

constexpr float SeaLevelPressure_HPa = 1026.6;  // Sea level pressure value to calculate actual altitude.

// ********************************************************************
// **************************** DEBUG SETTINGS ************************
// ********************************************************************
#define IGNORE_EEPROMS              // When defined, detected EEPROM chips are ignored, in order to avoid wasting write cycles during tests
// UNDEFINE THIS SYMBOL FOR OPERATIONS (when EEPROMs are used). Actual presence of EEPROMS is configured in the Hardware section below.

#define PRINT_DIAGNOSTIC_AT_INIT    // Undefine to avoid startup diagnostic if memory usage must be reduced.
#define INIT_SERIAL					// Undefine to reduce program size if there is no use
									// for the serial port at all (no debugging, no diagnostic at startup, etc.).

// Comment definition out to reduce program size if there is no use
// for the serial port at all (no debugging, no diagnostic at startup, etc.).
// AcquisitionProcess
#undef PRINT_ACQUIRED_DATA           // Define to have data printed in very readable format (DEBUGGING ONLY)
#undef PRINT_ACQUIRED_DATA_CSV       // Define to have data printed in CSV (1 line/record) format (DEBUGGING ONLY)

// Warning: if a compilation unit does NOT include this file, it could include DebugCSPU.h with other
// definition of DEBUG_CSPU, USE_ASSERTIONS, USE_TIMER. If we undefine DEBUG_CSPU, USE_ASSERTIONS and
// USE_TIMER here, there will be a link problem if it is defined in any compilation unit.
// Let those symbols defined here unless there is a memory shortage issue.
#define DEBUG_CSPU               // Comment out to avoid including any debugging code. DEFINE FOR OPERATION
#define USE_ASSERTIONS           // Comment out to avoid including assertions code
#undef USE_TIMER                 // Comment out to avoid timing output and overhead.
#include "DebugCSPU.h"           // include AFTER defining/undefining DEBUG, USE_ASSERTIONS and USE_TIMER.
#include "Timer.h"				 // Include even if USE_TIMER is undefined: we need the macro's anyway

constexpr bool ActivateStartCampaignPin=true;
								// If true, the StartCampaignPin defined in the hardware
								// section is used to trigger manual start campaign event.
								// This has no impact if the StartCampainPin is set to 0.

// Enable/disable the various parts of the debugging of the main loop here:
// 1. This group of debug settings must be set to 0 for operation
#define DBG_SETUP 1
#define DBG_INIT  0   			// Used in CansatAcquisitionProcess.
#define DBG_LOOP  0
#define DBG_LOOP_MSG  1
#define DBG_ACQUIRE 0			// Used in CansatAcquisitionProcess.
#define DBG_STORAGE 0			// Used in StorageManager.
#define DBG_SLOW_DOWN_TRANSMISSION 0   // Add a 500 msec delay in the RF transmission

// 2. This group of debug settings should preferably be set to 1 for operation,
//    unless there is a memory shortage.
#define DBG_CAMPAIGN_STARTED 1
#define DBG_DIAGNOSTIC 1		// Used in many classes for output of messages in case of
								// errors.

// **********************************************************************
// ****************** Primary mission parameters ************************
// **********************************************************************

#if defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS)
constexpr unsigned int CansatAcquisitionPeriod = 200;  	  // Acquisition period in msec.
#else
// With IB M4, in order to use a smaller battery, we use use an end-device XBee module.
// This somehow limits the admissible transmission period to 90 msec or more.
constexpr unsigned int CansatAcquisitionPeriod = 200;  	  // Acquisition period in msec.

#endif
// check carefully the actual duration of the acquisition+storage+transmission.
// Ditto for PRINT_ACQUIRED_DATA or any time-consuming feature.
constexpr unsigned int CansatCampainDuration = 300;   // Expected duration of the measurement campaign in seconds.
constexpr unsigned int CansatSensorsWarmupDelay = 1000; // Delay in msec to allow for the sensors to be ready.
constexpr byte CansatPrintEvery_X_Records = 50; // If campaign not started, 1 out of how many records should be sent on RF (and printed)

constexpr bool StartMeasurementCampaignImmediately=false; // If true, measurement campaign is started immediately.
											  // This should be false in operation.
// Detection of take-off ("start campaign condition")
constexpr float AltitudeThresholdToStartCampaign = 600.0f;
// The altitude above which the campaign is considered as
// started, whatever the speed conditions.
constexpr float MinSpeedToStartCampaign = 3.0f ;       // Minimum speed (m/s) to detect during NumSamplesToStartCampaign consecutive
// samples. Warning: MinSpeedToStartCampaign may not be > 10 m/s.
constexpr byte NumSamplesToAverageForStartingCampaign = (1000 / CansatAcquisitionPeriod);
// Number of samples used to average the vertical velocity to detect the take off.
// WARNING: sensor accuracy is +- 1 m/s, so noise cause detection of
// apparent speed of 1/acquisitionPeriod m/S. For
// acquisition period = 200 msec, this can be 5 m/s ! It is therefore
// essential to have NumSamplesToStartCamapaign cover at least 1 second.

// BMP_Client operational parameters
#define BMP_USE_BMP3XX_MODEL 	// If defined, a BMP3XX sensor is assumed
								// else a BMP280 is assumed.
constexpr uint8_t BMP_NumSamplesForPressureReading=1; // Number of averaged sample at each reading of pressure.
													  // Experience shows no advantage in reading more than once.
constexpr uint16_t BMP_NumMsecForDescentVelocityCalculation=500;
													  // Duration, in msec, of the minimum duration over which
													  // Descent velocity calculation is computed.
constexpr uint8_t BMP_NumPeriodsForDescentVelocityCalculation=
		constexpr_ceil((double) BMP_NumMsecForDescentVelocityCalculation/CansatAcquisitionPeriod);
													  // Number of cycles used for descent velocity calculation
constexpr uint8_t BMP_NumDescentVelocitySamplesToAverage=12;
													  // Number of samples used for desc. velocity moving average.

// CansatRecord configuration
// 1. Altitude & velocity information.
#undef  INCLUDE_GPS_VELOCITY   	 	// Define to transport GPS velocity data.
#define	INCLUDE_REFERENCE_ALTITUDE	// Define to transport the reference altitude i.e. the last
									// stable altitude, assumed to be the launchpad altitude.
constexpr float DefaultReferenceAltitude=562.0; // The default reference altitude which will be used at startup.
			// It should approximately equal the expected reference altitude.
			// 562 m = Elsenborn launch site.
constexpr float NoAltitudeChangeTolerance=2.0; // The max. altitude change during  NoAltitudeChangeDurationForReset
											   // considered as no altitude change.
constexpr uint32_t NoAltitudeChangeDurationForReset=60000; // msec. The duration used to defined whether altitude is stable.

#define INCLUDE_DESCENT_VELOCITY    // Define to transport (and acquire) the descent velocity.
// 2. Number of decimals to use in CSV representation are controlled by constants defined
//    in CansatRecord.h and which cannot be changed without altering storage types as well.


// Thermistors configuration (analog pin allocation is in the Hardware section of this file)
// WARNING: the configuration of the INCLUDE_THERMISTORn symbols must be
//          identical for all boards handling CansatRecords, otherwise the
//          record sizes will be incompatible.
constexpr float ThermistorTension=3.3;		// Tension used to power all thermistors (output of regulator if any).

constexpr float Thermistor1_Resistor=10000;	// Serial resistor for thermistor 1.
#define THERMISTOR1_CLASS ThermistorGT103J1K // The class used for thermistor 1.

#define INCLUDE_THERMISTOR2		// undefine if thermistor2 is not used.

#ifdef INCLUDE_THERMISTOR2
#  define THERMISTOR2_CLASS ThermistorNTCLE300E3502SB // The class used for thermistor 2.
   constexpr float Thermistor2_Resistor=10000;	// Serial resistor for thermistor 2.
#endif

#undef INCLUDE_THERMISTOR3		// undefine if thermistor3 is not used.
   	   	   	   	   	   	   	    // Warning: on ItsyBitsy M4, pin A2 is used for
   	   	   	   	   	   	   	   	//          Serial2!
#define THERMISTOR3_CLASS ThermistorNTCLE305E4103SB // The class used for thermistor 3.
#ifdef INCLUDE_THERMISTOR3
constexpr float Thermistor3_Resistor=10000;	// Serial resistor for thermistor 3.
#endif

// SD-card storage
#ifdef ARDUINO_ITSYBITSY_M4
#define CANSAT_IGNORE_SD_CARD		// Define to deactivate SD card storage by the storage manager.
#else
#undef CANSAT_IGNORE_SD_CARD		// Define to deactivate SD card storage by the storage manager.
#endif

constexpr unsigned int SD_RequiredFreeMegs = 0;     // The number of free Mbytes required on the main SD card at startup (0=do not check).
#define SD_FilesPrefix "cst_" 			// The prefix to use for the files on SD-Card
#define SD_FilesExtension "txt"			// The extension (3 chars) to use for the files on SD-Card
constexpr uint16_t EEPROM_KeyValue = 0x1217;          // The key identifying a valid EEPROM header.

constexpr unsigned long RT_CommanderTimeoutInMsec = 15000L; // The inactivity duration after which the can resumes the acquisition mode
constexpr unsigned long ReportingPeriodWhileWaitingForStartCampaign = 400; // msec. Period for reporting average speed and other
// parameters while waiting for take-off.

// **************** XBee modules configuration ******************

#define RF_ACTIVATE_API_MODE		// Define to perform all RF communication in API mode.

constexpr uint16_t XBeeChannels=0b0100000000000000;
		// Each bit from LSB to MSB activate a channel from 11 to 26.
		// Do not use channel 26 which has specific power limitations.
// If the XBee's SLEEP_RQ pin is connected, the µC pin is configured as pin_XBeeSleepRequest
// in the hardware section, below.
constexpr bool UseXBeeSleepMode=true; // If true, put XBee module to sleep after each frame
									  // emitted by the XBee. This has now inpact on boards
									  // for which pin_XBeeSleepRequest is set to 0
constexpr uint8_t XBeeNH_Parameter=2; // The number of unicast hops in a communication.
									  // If using only can & ground, set to 1, else set to
									  // appropriate value according to the longest route in the
									  // network. Warning the communication time-out is (NH*50msec)+100 msec.
									  // Larger values will slow down communication in case a module is
									  // out of reach or down.

// Addresses of our XBee modules
constexpr uint32_t XBeeAddressSH_01 = 0x0013a200, XBeeAddressSL_01 = 0x418FC759;
constexpr uint32_t XBeeAddressSH_02 = 0x0013a200, XBeeAddressSL_02 = 0x415E655F;
constexpr uint32_t XBeeAddressSH_03 = 0x0013a200, XBeeAddressSL_03 = 0x418FBBEB;
constexpr uint32_t XBeeAddressSH_04 = 0x0013a200, XBeeAddressSL_04 = 0x418FC78B; // Defective?
constexpr uint32_t XBeeAddressSH_05 = 0x0013A200, XBeeAddressSL_05 = 0x4175CA74;
constexpr uint32_t XBeeAddressSH_06 = 0x0013a200, XBeeAddressSL_06 = 0x4109F944; // Defective?
constexpr uint32_t XBeeAddressSH_07 = 0x0013a200, XBeeAddressSL_07 = 0x00000000; // Unused
constexpr uint32_t XBeeAddressSH_08 = 0x0013a200, XBeeAddressSL_08 = 0x418FC507;
constexpr uint32_t XBeeAddressSH_09 = 0x0013a200, XBeeAddressSL_09 = 0x41827f67;
constexpr uint32_t XBeeAddressSH_10 = 0x0013a200, XBeeAddressSL_10 = 0x418fb90a;


#define RF_XBEE_MODULES_SET	'A'		// Define for the XBee pair in use.
// Valid values:'A', 'B','C', 'D' or 'T' (=TEST)

#ifdef RF_ACTIVATE_API_MODE
#  if (RF_XBEE_MODULES_SET=='A')
constexpr uint32_t CanXBeeAddressSH = XBeeAddressSH_01 ; // Address of can XBee module (API mode)
constexpr uint32_t CanXBeeAddressSL =  XBeeAddressSL_01;
constexpr uint32_t GroundXBeeAddressSH = XBeeAddressSH_08; // Address of RF-Transceiver XBee module (API mode)
constexpr uint32_t GroundXBeeAddressSL = XBeeAddressSL_08;
#  elif (RF_XBEE_MODULES_SET=='B')
constexpr uint32_t CanXBeeAddressSH = XBeeAddressSH_03; // Address of can XBee module (API mode)
constexpr uint32_t CanXBeeAddressSL = XBeeAddressSL_03;
constexpr uint32_t GroundXBeeAddressSH = XBeeAddressSH_04; // Address of RF-Transceiver XBee module (API mode)
constexpr uint32_t GroundXBeeAddressSL = XBeeAddressSL_04;
#  elif (RF_XBEE_MODULES_SET=='C')
constexpr uint32_t CanXBeeAddressSH = XBeeAddressSH_05; // Address of can XBee module (API mode)
constexpr uint32_t CanXBeeAddressSL = XBeeAddressSL_05;
constexpr uint32_t GroundXBeeAddressSH =  XBeeAddressSH_06; // Address of RF-Transceiver XBee module (API mode)
constexpr uint32_t GroundXBeeAddressSL =  XBeeAddressSL_06;
#  elif (RF_XBEE_MODULES_SET=='D')
constexpr uint32_t CanXBeeAddressSH = XBeeAddressSH_07; // Address of can XBee module (API mode)
constexpr uint32_t CanXBeeAddressSL = XBeeAddressSL_07;
constexpr uint32_t GroundXBeeAddressSH =  XBeeAddressSH_02; // Address of RF-Transceiver XBee module (API mode)
constexpr uint32_t GroundXBeeAddressSL =  XBeeAddressSL_02;
#  elif (RF_XBEE_MODULES_SET=='T')					// Test modules
constexpr uint32_t CanXBeeAddressSH = XBeeAddressSH_09; // Address of can XBee module (API mode)
constexpr uint32_t CanXBeeAddressSL = XBeeAddressSL_09; // The one without antenna
constexpr uint32_t GroundXBeeAddressSH =  XBeeAddressSH_10; // Address of RF-Transceiver XBee module
constexpr uint32_t GroundXBeeAddressSL = XBeeAddressSL_10; // (the one with antenna)

#  else
#    error "Unknown XBee set in CansatConfig.h"
#  endif // RF_USE_FINAL_XBEE_MODULES
#else
constexpr uint8_t MaxUplinkMsgLength=150; // The maximum number of characters in an uplink message
										  // in transparent mode (cmd to the RT-Commander).
#endif // RF_ACTIVATE_API_MODE

// Power levels for the XBee module before and during campaign.
#ifdef ARDUINO_ITSYBITSY_M4
// Only save power with ItsyBitsy M4 (currently used with a smaller battery
constexpr XBeeTypes::TransmissionPowerLevel OutOfCampaignPowerLevel=XBeeTypes::TransmissionPowerLevel::level0;
#else
constexpr XBeeTypes::TransmissionPowerLevel OutOfCampaignPowerLevel=XBeeTypes::TransmissionPowerLevel::level4;
#endif
constexpr XBeeTypes::TransmissionPowerLevel InCampaignPowerLevel=XBeeTypes::TransmissionPowerLevel::level4;

// ******************** END XBEE CONFIGURATION ************************

constexpr unsigned int CansatGPS_Frequency=5; // GPS update frequency in Hz (1, 5 or 10).
// Define whether the GPS should be disabled after a first fix is acquired,
// when the measurement campaign is not started. It will actually only be disabled
// if: 	- the enable pin of the GPS is actually connected to a pin of the µC
//		- constant pin_GPS_Enable is properly configured.
#ifdef ARDUINO_ITSYBITSY_M4
constexpr bool CansatGPS_DisableBeforeCampaign=true;
#else
constexpr bool CansatGPS_DisableBeforeCampaign=false;
#endif

// NB: See allocation of GPS Enable pin in hardware configuration section.

// ********************************************************************
// **************** Secondary mission configuration  ******************
// ********************************************************************

// Period to be used to manage the secondary mission (used by the
// SecondaryMissionController). Since it is evaluated once during each
// acquisition cycle, it does not make sense to have it smaller than
// CansatAcquisitionPeriod.
constexpr unsigned int CansatSecondaryMissionPeriod=CansatAcquisitionPeriod;

// ********************************************************************
// ********************* Hardware configuration  **********************
// ********************************************************************

// EEPROMs
#define NO_EEPROMS_ON_BOARD		     // Define to turn off all EEPROM-related features.
#ifdef NO_EEPROMS_ON_BOARD
#undef CANSAT_USE_EEPROMS            // Undefine if the can does not include EEPROMS.
#else
#define CANSAT_USE_EEPROMS
#endif

// Serial ports
constexpr  long USB_SerialBaudRate = 115200;              // baudrate on the serial interface of the µC board. Faster is best.
constexpr  long RF_SerialBaudRate  = 115200;              // baudrate to communicate with the RF board (must be consistent with XBee settings.
constexpr  byte GPS_SerialPortNumber = 2;
constexpr  byte RF_SerialPortNumber  = 1;

// Analog pins allocation & reference tension
#if (defined(ARDUINO_ARCH_SAMD) && defined(__SAMD51__))
constexpr bool CansatUsesExternalReference = false;
  /* On the SAMD51 processors (both Feather M4 Express and ItsyBitsy M4), a silicon
   * bug prevents using an external reference (see Adafruit website for details).
   */
#else
constexpr bool CansatUsesExternalReference = false; // If true, the ADC uses an external reference
#endif
constexpr byte Thermistor1_AnalogInPinNbr = A0 ;     // Analog input for thermistor 1
constexpr byte Thermistor2_AnalogInPinNbr = A1 ;     // Analog input for thermistor 2
#ifndef __SAMD51__
constexpr byte Thermistor3_AnalogInPinNbr = A2 ;     // Analog input for thermistor 3.
													 // Warning: on ItsyBitsy M4, pin A2 is used for
													 //          Serial2!
constexpr byte unusedAnalogInPinNumber = A3; // An unused analog pin. Value read is used as random seed.
											 // Warning: on ItsyBitsy M4, pin A3 is used for
											 //          Serial2!
#else
constexpr byte Thermistor3_AnalogInPinNbr = A4 ;
constexpr byte unusedAnalogInPinNumber = A5;
#endif
constexpr byte DebugCtrlPinNumber = A5; 	 // NOT USED SINCE 2021-22. The pin used to force the initialization of the USB link.
											 // Keep for backward compatibility.

// I2C Addresses
constexpr byte I2C_lowestAddress = 0x10;
constexpr byte I2C_highestAddress = 0x7F;   // EEPROMS are at 0x50-0x57.
constexpr byte I2C_BMP_SensorAddress = 0x77;
constexpr uint16_t WireLibBitRateInKhz = 400;

// Digital pins allocation
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
constexpr byte SD_CardChipSelect = 4;		// Internal SD-Card reader on AdaLogger
constexpr byte StartCampaignPin  = A5;		// For debugging only. When this pin is pulled to GND,
											// if forces a "start or stop campaign" command.
											// Set to 0 to disable. This is used only if
											// ActivateStartCampainPin is true.
#elif defined(ARDUINO_ITSYBITSY_M4)
constexpr byte SD_CardChipSelect = 0;		// No SD-Card reader on ItsyBitsy M4
constexpr byte StartCampaignPin  = 10;		// For debugging only. When this pin is pulled to GND,
											// if forces a "start or stop campaign" command.
											// Set to 0 to disable. This is used only if
											// ActivateStartCampainPin is true
#endif


#if (defined ARDUINO_AVR_UNO || defined ARDUINO_AVR_MICRO)
// This is an alternate configuration for development using a standard UNO board.
constexpr byte pin_HeartbeatLED = LED_BUILTIN;
constexpr byte pin_InitializationLED = 8;
constexpr byte pin_AcquisitionLED = 0;
constexpr byte pin_StorageLED = 0;
constexpr byte pin_TransmissionLED = 0;
constexpr byte pin_CampaignLED = 8;
constexpr byte pin_UsingEEPROM_LED = 0;
constexpr byte pin_GPS_Enable=0; // 0 = not connected, GPS always enabled.
constexpr int defaultDAC_StepNumber = 1023;
constexpr byte pin_XBeeSleepRequest = 0; // 0 = not connected, XBee always enabled.
constexpr byte pin_XBeeOnSleep = 0; // 0 = not connected.

constexpr float referenceVoltage = 5;

#elif (defined ARDUINO_SAMD_FEATHER_M0_EXPRESS) || (defined ARDUINO_ITSYBITSY_M4)
// This is the operational target hardware.
constexpr byte pin_HeartbeatLED = LED_BUILTIN;
constexpr byte pin_InitializationLED = 0; // On during initialization (unused)
constexpr byte pin_AcquisitionLED = 0;	  // Our cycle is too short to use all LEDs.
constexpr byte pin_StorageLED = 0;
constexpr byte pin_TransmissionLED = 0;
constexpr byte pin_CampaignLED = 0;		  // On until campaign started. Built-in green led on Adalogger
constexpr byte pin_UsingEEPROM_LED = 0;
constexpr int defaultDAC_StepNumber = 4095;
constexpr float referenceVoltage = 3.3;

#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
constexpr byte pin_GPS_Enable=0; // 0 = not connected, GPS always enabled.
constexpr byte pin_XBeeSleepRequest = 0; // 0 = not connected, XBee always enabled.
constexpr byte pin_XBeeOnSleep = 0; // 0 = not connected.
#else
constexpr byte pin_GPS_Enable=11; // 0 = not connected, GPS always enabled.
constexpr byte pin_XBeeSleepRequest = 12; // 0 = not connected, XBee always enabled.
constexpr byte pin_XBeeOnSleep = 9; // 0 = not connected.
#endif

#else
#error "Unexpected board. Please use either TMinus, Feather M0 Express, ItsyBitsy M4 or Uno, or complete the CansatConfig.h or the project's configuration file "
#endif
