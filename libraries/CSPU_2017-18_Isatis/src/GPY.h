#pragma once
#include"Arduino.h"
class GPY {
  public:
    static double getOutputV(const byte dataOutPinNbr, const byte LED_VccPinNbr);
    static double getOutputV_Average( const byte dataOutPinNbr, 
                                      const byte LED_VccPinNbr,
                                      const byte numSamples, 
                                      const double GPY_MaxDeltaInAverageSet,
                                      const bool repeatBadQualityReadings,
                                      bool &qualityOk);
    static void setFanOn(bool status); // turn fan on if status==true, off if status==false.
};

