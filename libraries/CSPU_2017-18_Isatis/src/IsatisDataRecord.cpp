#include "IsatisDataRecord.h"

void IsatisDataRecord::print(Stream& str) const {
  str.print(F("Start reading  :"));
  str.println(startTimestamp);
  str.print(F("GPY_OutputV    :"));
  str.print(GPY_OutputV,numDecimalPositionsToUse);
  str.print(F(" (Q:"));
  str.print(GPY_Quality);
  str.println(F(")"));
#ifndef USE_MINIMUM_DATARECORD
  str.print(F("GPY_Voc        :"));
  str.println(GPY_Voc,numDecimalPositionsToUse);
  str.print(F("GPY_DustDensity:"));
  str.println(GPY_DustDensity,numDecimalPositionsToUse);
  str.print(F("GPY_AQI        :"));
  str.println(GPY_AQI,numDecimalPositionsToUse);
#endif
  str.print(F("Temperature    :"));
  str.println( BMP_Temperature,numDecimalPositionsToUse);
  str.print(F("Altitude       :"));
  str.println(BMP_Altitude,numDecimalPositionsToUse );
  str.print(F("Pressure (hPa) :"));
  str.println(BMP_Pressure,numDecimalPositionsToUse);
  str.print(F("Stop reading   :"));
  str.println(endTimestamp);
}

void IsatisDataRecord::printCSV(Stream& str, bool slow) const {
  str << startTimestamp << F(",");
  str.print(GPY_OutputV,numDecimalPositionsToUse);
  if (slow) delay(RF_TransmissionDelayEveryTwoNumbers);
  str <<  F(",");
  str.print(GPY_Quality);
  str << F(","); 
  if (slow) delay(RF_TransmissionDelayEveryTwoNumbers);
#ifndef USE_MINIMUM_DATARECORD
  str.print(GPY_Voc,numDecimalPositionsToUse);
  str <<  F(",");
  str.print(GPY_DustDensity,numDecimalPositionsToUse);
  str <<  F(",") ;
  str.print(GPY_AQI,numDecimalPositionsToUse);
  str <<  F(",") ;
  if (slow) delay(RF_TransmissionDelayEveryTwoNumbers);
#endif
  str.print( BMP_Temperature,numDecimalPositionsToUse);
  str <<  F(",") ;
  str.print(BMP_Altitude,numDecimalPositionsToUse );
  str <<  F(",") ;
  if (slow) delay(RF_TransmissionDelayEveryTwoNumbers);
  str.print(BMP_Pressure,numDecimalPositionsToUse);
   str <<  F(",") ;
  str.print(endTimestamp);
}

void IsatisDataRecord::printCSV_Header(Stream& str) const {
  str.print(F("start,GPY_outputV, GPY_Quality"));
#ifndef USE_MINIMUM_DATARECORD
  str.print(F(",GPY_Voc,dustDensity,AQI"));
#endif
  str.print(F(",Temp,Alt,Press,end"));
}

void IsatisDataRecord::clear() {
  GPY_OutputV = 0.0;
  GPY_Quality=false;
#ifndef USE_MINIMUM_DATARECORD
  GPY_Voc=0.0;
  GPY_DustDensity=0.0;
  GPY_AQI=0.0;
#endif
  BMP_Temperature = 0.0;
  BMP_Pressure = 0.0;
  BMP_Altitude = 0.0;
  startTimestamp=0;
  endTimestamp=0;
}



