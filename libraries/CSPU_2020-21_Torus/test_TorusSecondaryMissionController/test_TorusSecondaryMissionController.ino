/* 
 *  Test program for TorusSecondaryMissionController class
 *
 *  Resulting records are stored on SD card.
 *  Define DBG_
 *
 *  Requirements:
 *  	- SD Card reader (defaults to the on-board reader of Feather M0 Adalogger
 *  	- data file I2_10mDV.csv, located in folder
 *  	  testData must be in the SD-Card root directory.
 */

#define DEBUG_CSPU		  // Define before any include.
#include "TorusServoWinch.h"
#include "CansatRecordTestFlow.h"
#include "TorusSecondaryMissionController.h"
#include "TorusRecord.h"
#include "SD_Logger.h"
#include "DebugCSPU.h" // Include this as last to avoid undefined symbol issue
					   // on faiCSPU() during link.
#include "TestUtilityFunctions.h"

bool stopAfterFailedTest=false;
bool stopAfterFirstFailedCheck=false;

byte SD_Pin = 10;

TorusSecondaryMissionController controller;
TorusServoWinch servo;
CansatRecordTestFlow recordFlow;
TorusRecord record;
SD_Logger logger(SD_Pin); // previously: SD_Logger logger(4);
uint32_t numErrors=0;
uint32_t recordCounter;


void TestReferenceAltitudeCalculation() {
	recordCounter=0;
	randomSeed(analogRead(0));

	record.clear();
	uint32_t start=100000;
	float ground=500;
	waitEndOfStartup(); // Just to avoid "Starting up" debug messages.
	record.timestamp=start;
	Serial << ENDL << "===== Testing reference altitude calculation ===== " << ENDL;
	Serial << "*** Test of reference altitude should be moved to BMP_Client when final ***" << ENDL;

	// 0. Allow for a first set of the reference altitude.
	Serial <<  ENDL << "0. Processing enough records to initialize reference altitude..." << ENDL;
	while ((record.timestamp -start)< 1.5*NoAltitudeChangeDurationForReset) {
		RunA_RecordForRefAltitudeTest(record, ground);
	}
	// 1. Simulate situation before take-off:  random changes [x-0,75:x+0,75]
	//    with periods of slow change when can is moved.
	Serial << ENDL << "1. Simulating can being moved on the ground..." << ENDL;
	for (int k=0 ; k < 5; k++) {
		// 1a no change except for noise.
		Serial <<  ENDL << "Cycle " << k+1 << " of 5. Ground altitude is " << ground << "m" << ENDL;
		start=record.timestamp;
		uint32_t duration=3.1*NoAltitudeChangeDurationForReset;
		Serial << "Not moving except noise for " << duration/1000 << " seconds..." << ENDL;
		while ((record.timestamp -start)< duration) {
			RunA_RecordForRefAltitudeTest(record, ground);
			if (fabs(record.refAltitude - ground) > 0.3) {
				Serial << "*** Error: refAltitude=" << record.refAltitude << " while ground=" << ground << ENDL;
				numErrors++;
			}
		}
		float currentRefAltitude=record.refAltitude;
		Serial <<  ENDL << "Moving slowly..." << ENDL;
		// 1b Move at 1.5 m/s during about 10 s
		// at this rate, ref altitude should not change.
		for (unsigned int i =0; i< (10000 / CansatAcquisitionPeriod); i++) {
			ground+= 1.5*(CansatAcquisitionPeriod/1000.0);
			RunA_RecordForRefAltitudeTest(record,ground);
			if (fabs(record.refAltitude - currentRefAltitude) > 0.0001) {
				Serial << "*** Error: refAltitude changed to " << record.refAltitude << " while moving at 1.5 m/s" << ENDL;
				numErrors++;
			}
		} // for i
		start=record.timestamp;
		Serial << ENDL << "Not moving to allow for ref. altitude to be updated... ground at " << ground << "m" << ENDL;
		// 1c do not move during enough time for ref altitude to be updated.
		while ((record.timestamp -start)< 1.1*NoAltitudeChangeDurationForReset) {
				RunA_RecordForRefAltitudeTest(record,ground);
		}
	} // for k

	// 2. Simulate take-off: ascent at 20 m/s up to 1000 m
	Serial << ENDL << "2. Taking off from " << ground << "m ..." << ENDL;
	float currentRefAltitude=record.refAltitude;
	float altitude=ground;
	while (altitude < (ground+1000.0)) {
		altitude+=20*(CansatAcquisitionPeriod/1000.0);
		RunA_RecordForRefAltitudeTest(record,altitude);
		if (fabs(record.refAltitude - currentRefAltitude) > 0.3) {
			Serial << "*** Error: refAltitude=" << record.refAltitude << " while ground=" << ground << ENDL;
			numErrors++;
		}
	}

	Serial << ENDL << "3. Now at " << altitude << "m... Ejecting...." << ENDL;
	// 3. Simulate ejection: descent velocity increased up to 10 m/s at
	//    10 m/s^2 + noise +-1 m/s
	float velocity=0;
	uint32_t ejectionTs=record.timestamp;
	while (velocity < 10.0) {
		velocity+= 10*(CansatAcquisitionPeriod/1000.0);
		velocity+= (random(0,200)/100.0 - 1.0);
		altitude-=velocity*(CansatAcquisitionPeriod/1000.0);
		RunA_RecordForRefAltitudeTest(record,altitude);
		if (fabs(record.refAltitude - currentRefAltitude) > 0.3) {
			Serial << "*** Error: refAltitude=" << record.refAltitude << " while ground=" << ground << ENDL;
			numErrors++;
		}
	}

	Serial << ENDL << "4. Descent velocity now " << velocity << " m/s, altitude=" << altitude << ".... descending..." << ENDL;
	// 4. Simulate descent
	while (altitude > ground) {
		altitude-=velocity*(CansatAcquisitionPeriod/1000.0);
		RunA_RecordForRefAltitudeTest(record,altitude);
		if (fabs(record.refAltitude - currentRefAltitude) > 0.3) {
			Serial << "*** Error: refAltitude=" << record.refAltitude << " while ground=" << ground << ENDL;
			numErrors++;
		}
	}
	Serial << ENDL << "Can landed after descending during " << (record.timestamp-ejectionTs)/1000.0 << "sec." << ENDL;
	Serial << "altitude=" << altitude << ", ground=" << ground << ", ref.altitude=" << record.refAltitude << ENDL;

	printTestSummary();
}

void TestReferenceAltitudeCalculationWithCmdMode() {
	Serial << ENDL << ENDL;
	Serial << "==== Checking effect of CommandMode interruption on ref. altitude calculation =====" << ENDL;
	record.clear();
	uint32_t start=100000;
	float ground=500;
	record.timestamp=start;
	// 0. Allow for a first set of the reference altitude.
	Serial <<  ENDL << "0. Processing enough records to initialize reference altitude..." << ENDL;
	while ((record.timestamp -start)< 1.5*NoAltitudeChangeDurationForReset) {
		RunA_RecordForRefAltitudeTest(record,ground);
	}

	Serial << ENDL << "1. Simulating 30 seconds command mode phases interrupting acquisition while can does not move" << ENDL;
	Serial << "  Ground at " << ground << "m" << ENDL;
	float currentRefAltitude=record.refAltitude;
	for (int i = 0; i < 5 ; i++) {
		record.timestamp+=30000;
		start = record.timestamp;
		while ((record.timestamp -start)< 1.5*NoAltitudeChangeDurationForReset) {
			RunA_RecordForRefAltitudeTest(record,ground);
			if (fabs(record.refAltitude - currentRefAltitude) > 0.3) {
				Serial << "*** Error: refAltitude=" << record.refAltitude << " while ground=" << ground << ENDL;
				numErrors++;
			}
		}
	}

	Serial << ENDL << "2. Simulating 30 seconds command mode phases interrupting acquisition while can does move" << ENDL;
	Serial << "  Ground at " << ground << "m, moving slowly (1.5 m/s during about 10 s during interruption, i.e. 15 m shift)" << ENDL;
	for (int i = 0; i < 5 ; i++) {
		record.timestamp+=30000;
		ground+=15;
		start=record.timestamp;
		while ((record.timestamp -start)< 1.5*NoAltitudeChangeDurationForReset) {
			RunA_RecordForRefAltitudeTest(record,ground);
		}
		if (fabs(record.refAltitude - ground) > 0.3) {
			Serial << "*** Error: refAltitude=" << record.refAltitude << " while ground=" << ground << ENDL;
			numErrors++;
		}
	}

	Serial << ENDL
		   << "3. Simulating a problematic timing: " << ENDL;
	Serial << "   - cause a reset of the interval with an large altitude change just before" << ENDL;
	Serial << "     interruption, " << ENDL;
	Serial << "   - first record with same altitude after interruption longer than" << ENDL;
	Serial << "     the monitored delay." << ENDL;
	Serial << "   It should NOT cause a change in ref. altitude, since altitude was not continuously observed." << ENDL;
	ground+=10;
	RunA_RecordForRefAltitudeTest(record,ground);
	currentRefAltitude=record.refAltitude;
	record.timestamp+=1.5*NoAltitudeChangeDurationForReset;
	RunA_RecordForRefAltitudeTest(record,ground);
	if (fabs(record.refAltitude - currentRefAltitude) > 0.3) {
		Serial << "*** Error: ref altitude should not have been altered. " << ENDL;
		Serial << "           refAltitude=" << record.refAltitude << " previous value=" << currentRefAltitude << ENDL;
		numErrors++;
	}

	printTestSummary();
}

void ProcessFile(const char* fileName, String& resultFile) {
	recordCounter=0;
	logger.init("tst_");
	recordFlow.openInputFile(fileName, SD_Pin);
	Serial << ENDL << "Testing with file '" << fileName << "'" << ENDL;
	Serial << "Processing at the actual speed, according to record timestamps (be patient...)" << ENDL;
	resultFile=logger.fileName();
	Serial << "Saving records to '"<< resultFile << "'" << ENDL;

	if (!recordFlow.openInputFile(fileName, SD_Pin)) {
		Serial << "Cannot read from file '" << fileName << "'. Test aborted" << ENDL;
		numErrors++;
		return;
	}
	waitEndOfStartup();
	float lastAltitude=-10000;
	float lastDescentVelocity=-10000;
	TorusControlCode lastCtrlInfo=TorusControlCode::NoData;
	uint8_t lastPhase=255;
	uint16_t lastTarget=65000;
	uint32_t localCounter=0;
	elapsedMillis cycleDuration=0;
	elapsedMillis processingDuration=0;
	uint32_t previousRecTS=0;
	uint32_t savedProcessingDuration;

	while (recordFlow.getRecord(record)) {
		localCounter++;
		recordCounter++;
		processingDuration=0;
		servo.run();
		controller.run(record);
		savedProcessingDuration=processingDuration;
		if (savedProcessingDuration > 1) {
			Serial << "*** Processing duration suspiciously long:"<< savedProcessingDuration << " msec)" << ENDL;
			Serial << "    recordCounter=" << recordCounter  << ENDL;
		}
		if ( (fabs(lastAltitude - record.altitude) > 10) ||
			 (fabs(lastDescentVelocity - record.descentVelocity) > 1.0) ||
			 (lastCtrlInfo != record.controllerInfo) ||
			 (lastPhase != record.flightPhase) ||
			 (lastTarget != record.parachuteTargetRopeLen) ||
			 localCounter >=20) {
			Serial << recordCounter << ": ";
			printSecondaryInfo(record);
			lastAltitude=record.altitude;
			lastDescentVelocity=record.descentVelocity;
			lastCtrlInfo=record.controllerInfo;
			lastPhase=record.flightPhase;
			lastTarget=record.parachuteTargetRopeLen;
			localCounter=0;
		}
		logger.log(record);

		// We need the loop to actually run with the period found in the records, in order for the
		// servo position to be adapted realistically.
		if (previousRecTS >0) {
			uint32_t expectedDuration=record.timestamp-previousRecTS;
			if (expectedDuration > cycleDuration) {
				delay(expectedDuration-cycleDuration);
			}
			else {
				Serial << "Info: cycle too slow (due to SD-Card operations) !! " << ENDL;
				Serial << "recordCounter=" << recordCounter << " expectedDuration=" << expectedDuration
					   << " cycleDur=" << cycleDuration
					   << ", actualProcessing (display and SD-card operation excluded)=" << savedProcessingDuration<< " msec" << ENDL;
			}
		}
		previousRecTS=record.timestamp;
		cycleDuration=0;
	}
	Serial << "Test over. Check output file '" << resultFile << "' on SD-Card" << ENDL;
}

void TestStartupConditions() {
	bool tested;
	record.clear();
	Serial << ENDL << ENDL << "==== Testing detection of startup condition.. " << ENDL;
	do {
		record.timestamp=millis();
		controller.run(record);
		if (record.timestamp < TorusMinDelayBeforeFlightControl )  {
			if (record.controllerInfo!=TorusControlCode::StartingUp){
				Serial << "*** Error: record with TS=" << record.timestamp << ", controllerInfo is NOT StartingUp" << ENDL;
				numErrors++;
			}
			if (record.flightPhase!=15){
				Serial << "*** Error: record with TS=" << record.timestamp << ", flightPhase is NOT 15 (NoPhase)" << ENDL;
				numErrors++;
			}
			tested=true;
		}
	} while ((millis() < TorusMinDelayBeforeFlightControl) && (!tested));
	if (!tested) {
		Serial << "*** Could not test detection of start-up condition" << ENDL;
		numErrors++;
	}
	printTestSummary();
}

void TestSafetyChecks() {
	/* This test is tricky: we want to test with many records, but records are
	 * not actually processed unless their timestamps are TorusMinDelayBetweenTargetUpdate
	 * apart. On the other end, keeping the altitude stable for that long
	 * cause the reference altitude to be updated, which is not the point.
	 * That's why we keep wobbling the altitude around.
	 */

	Serial << ENDL << ENDL << "==== Testing safety checks ==== " << ENDL;
    record.clear();
    record.altitude=450;
    setReferenceAltitude(record);
    waitEndOfStartup();

    // Ground condition detection:
    Serial << "1. Ground condition detection..." << ENDL;
    record.altitude=record.refAltitude-10; // start with negative relative altitude.
    record.timestamp=10000;
    printSecondaryInfo(record);
    uint32_t startTS=record.timestamp;
    do {
    	controller.run(record);
    	wobbleAltitude(record);
    	checkCtrlInfo(record,TorusControlCode::GroundConditions, "GroundConditions", 15);
    	checkTarget(record, ServoInitAndEndPosition);
    	record.timestamp+=CansatAcquisitionPeriod;
    	// Do not update until it has a chance to be taken into account.
    	if ((record.timestamp - startTS) > TorusMinDelayBetweenPhaseChange) {
    		record.altitude+=10 * (CansatAcquisitionPeriod/1000.0f);
    		startTS=record.timestamp;
    	}
    } while (record.altitude < TorusMinAltitude+record.refAltitude);

    // Not descending
    // a) Ascent
    Serial << "2a. Ascending..." << ENDL;
    // Make sure altitude wobbling will not get us back below TorusMinAltitude
    record.altitude+=2*NoAltitudeChangeTolerance;
    record.descentVelocity=-15;
    record.timestamp+=2*TorusMinDelayBetweenPhaseChange;
    startTS=record.timestamp;
    printSecondaryInfo(record);
    do {
    	controller.run(record);
    	wobbleAltitude(record);
    	checkCtrlInfo(record,TorusControlCode::NotDescending, "NotDescending", 15);
    	checkTarget(record, ServoInitAndEndPosition);
    	record.timestamp+=CansatAcquisitionPeriod;
    	if ((record.timestamp - startTS) > TorusMinDelayBetweenPhaseChange) {
    		record.descentVelocity+=0.5;
    		startTS=record.timestamp;
    	}
    } while (record.descentVelocity < 0.0f);
    // b) ejection
    Serial << "2b Recently ejected..." << ENDL;
    record.timestamp+=TorusMinDelayBetweenPhaseChange;
    startTS=record.timestamp;
    do {
    	controller.run(record);
    	wobbleAltitude(record);
    	checkCtrlInfo(record,TorusControlCode::NotDescending, "NotDescending", 15);
       	checkTarget(record, ServoInitAndEndPosition);
       	record.timestamp+=CansatAcquisitionPeriod;
       	if ((record.timestamp - startTS) > TorusMinDelayBetweenPhaseChange) {
       		record.descentVelocity+= 10*(CansatAcquisitionPeriod/1000.0f);
       		startTS=record.timestamp;
       	}
    } while (record.descentVelocity < TorusVelocityDescentLimit);

    // Velocity too low alert
    Serial << "3. Velocity to low alert..." << ENDL;
    Serial << "*** TMP: ADD CHECK ON ALERT REACTIONS WHEN DEFINED" << ENDL;
    record.timestamp+=TorusMinDelayBetweenPhaseChange;
    do {
       	controller.run(record);
       	checkCtrlInfo(record,TorusControlCode::LowDescentVelocityAlert, "LowVelocityAlert", 15);
        // ADD CHECK WHEN REACTION IS DEFINED
       	record.timestamp+=CansatAcquisitionPeriod;
       	record.descentVelocity+= 10*(CansatAcquisitionPeriod/1000.0f);
       } while (record.descentVelocity < TorusVelocityLowerSafetyLimit);


    // Descending in nominal speed range will be tested when testing flight phases

    // Velocity too high alert
    Serial << "4. Velocity to high alert..." << ENDL;
    record.descentVelocity= 25;
    record.timestamp+=TorusMinDelayBetweenPhaseChange;
    do {
    	controller.run(record);
    	checkCtrlInfo(record,TorusControlCode::HighDescentVelocityAlert, "HighVelocityAlert", 15);
    	// ADD CHECK WHEN REACTION IS DEFINED
    	record.timestamp+=CansatAcquisitionPeriod;
    	record.descentVelocity-= 0.5;
    } while (record.descentVelocity < 10);

    printTestSummary();
}

void TestFlightPhaseDetection(){
	Serial << ENDL << ENDL << "==== Testing flight phase detection ==== " << ENDL;
    record.clear();
    record.altitude=300.0;
    setReferenceAltitude(record);
    waitEndOfStartup();
    record.timestamp+=TorusMinDelayBetweenPhaseChange;
    controller.run(record);
    checkCtrlInfo(record,TorusControlCode::GroundConditions, "GroundCondition", 15);

    // flight from 1000 to 0 m., safe velocity.
    // phase detection can be delayed by at most TorusMinDelayBetweenPhaseChange msec.
    record.altitude=record.refAltitude+1000.0;
    record.timestamp+=(TorusMinDelayBetweenPhaseChange+1);
    record.descentVelocity = (TorusVelocityLowerSafetyLimit+TorusVelocityUpperSafetyLimit)/2;
    uint8_t currentPhase=255;
    uint8_t previousExpectedPhase=255;
    uint8_t expectedPhase=255;
    uint8_t tmp;
    int i;
    do {
    	controller.run(record);
    	// Print at every phase change.
    	if (record.flightPhase != currentPhase) {
    		Serial << "   Flight phase=" << record.flightPhase
    			   << " (relative altitude=" << record.altitude-record.refAltitude << "m)" << ENDL;
    		currentPhase= record.flightPhase;
    	}

    	// Define expected phase and keep previouslyExpected phase to deal with detection delay.
    	bool found=false;
    	for (i = 0; i<TorusNumPhases;i++) {
    		if ((record.altitude-record.refAltitude) > TorusPhaseAltitude[i]) {
    			found=true;
    			break;
    		}
    	}
    	if (found) {
    		tmp= (uint8_t) i;
    	}
    	else {
    		tmp=15;
    	}
    	if (tmp != expectedPhase) {
    		previousExpectedPhase=expectedPhase;
    		expectedPhase=tmp;
    	}
    	checkCtrlInfo(record,TorusControlCode::NominalDescent, "NominalDescent");
    	checkPhase(record, expectedPhase, previousExpectedPhase);
    	checkTarget(record, TorusPhaseRopeLen[record.flightPhase]);
    	record.timestamp+=CansatAcquisitionPeriod;
    	record.altitude-=record.descentVelocity*CansatAcquisitionPeriod/1000.0f;
    } while (record.altitude > (record.refAltitude+TorusPhaseAltitude[TorusNumPhases-1]));
    record.timestamp+=(TorusMinDelayBetweenPhaseChange+1);
    printSecondaryInfo(record);
    controller.run(record);
    checkCtrlInfo(record,TorusControlCode::GroundConditions, "GroundConditions", 15);

    printTestSummary();
}

void setup() {
	DINIT(115200);
	Serial << "Init OK" << ENDL;
	Serial << "  Acquisition period (from CansatConfig.h): " << CansatAcquisitionPeriod << " msec" << ENDL;
	Serial << "This test assumes the following settings:" << ENDL;
	Serial << "  NoAltitudeChangeDurationForReset=1 min";
	if (NoAltitudeChangeDurationForReset == 60000) {
		Serial << " OK" << ENDL;
	} else {
		Serial << ENDL << " *** Current value=" << NoAltitudeChangeDurationForReset << " msec";
		Serial << ": test data must be adapted!" << ENDL;
	}
	Serial << "  NoAltitudeChangeTolerance=2m";
	if ((NoAltitudeChangeTolerance - 2.0) < 0.001) {
		Serial << " OK" << ENDL;
	} else {
		Serial << ENDL << " *** Current value=" << NoAltitudeChangeTolerance ;
		Serial << ": test data must be adapted!" << ENDL;
	}
	servo.begin(9,ServoWinch_mmPerMove);
	controller.begin(servo);

	TestStartupConditions(); // This test must be the first one and happen as soon as possible after startup and init.

	// Testing referenceAltitude.
	TestReferenceAltitudeCalculation();
	TestReferenceAltitudeCalculationWithCmdMode();

	// Testing safety checks
	TestSafetyChecks();

	// Testing flight phase detection
	TestFlightPhaseDetection();

	// Testing with actual flight data
	String resultFile;
	Serial << ENDL << "Testing with IsaTwo actual flight data (16850 lines)..." << ENDL;
	ProcessFile("I2_10mDV.csv", resultFile);

	Serial << "Validated test results are in folder 'TestResults' and can be used to check the output of this test." << ENDL;

	Serial << ENDL << "End of job. Number of errors: " << numErrors << ENDL;
}

void loop() {
  // put your main code here, to run repeatedly
}
