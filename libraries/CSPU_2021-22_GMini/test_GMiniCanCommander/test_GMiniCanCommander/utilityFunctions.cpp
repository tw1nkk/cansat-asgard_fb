// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "CansatInterface.h"

extern int numErrors;

void requestVisualCheck(const char* msg, bool separatorFirst=true) {                 /**<Method used to request a visual check after an action.*/
  char answer = ' ';
  if (separatorFirst) {
    Serial << ENDL;
  }
  while (Serial.available() > 0) {
    Serial.read();
  }

  while ((answer != 'y') && (answer != 'n')) {
    Serial << ENDL << msg << F(". Is this ok(y/n) ? ");
    Serial.flush();
    while (Serial.available() == 0) {
      delay(300);
    }
    answer = Serial.read();
  }
  if (answer == 'n') {
    numErrors++;
  }
  Serial << ENDL<<ENDL;
}

void requestVisualCheckWithCmd(String &theCmd, const char* msg) {
  Serial << "Sent command '" << theCmd << "'" << ENDL;
  requestVisualCheck(msg, false); 
}

/* Build a request string with 0, 1 or 2 parameters (parameters are ignored if they are empty strings).  */
void buildRequest(String &theCmd, int reqType, const char* param1="", const char* param2="") {
  theCmd = reqType;
  if (strlen(param1) >0) {
    theCmd += ',';
    theCmd += param1;
  }
  if (strlen(param2) >0) {
    theCmd += ',';
    theCmd += param2;
  }
  Serial << "Command built for test: '" << theCmd << "'" <<ENDL;
}
void buildRequest(String &theCmd, CansatCmdRequestType reqType, const char* param1="", const char* param2="") {
  buildRequest(theCmd, (int) reqType, param1, param2);
}
