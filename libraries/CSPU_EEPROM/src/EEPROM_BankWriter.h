
#pragma once
#include "Arduino.h"
#include "HardwareScanner.h"
#include "VirtualMethods.h"
#include "elapsedMillis.h"
#include "ExtEEPROM.h"

/** @ingroup CSPU_EEPROM
 *  @brief A COMPLETER
 *  
 *  DESCRIPTION A COMPLETER.
 */
class EEPROM_BankWriter {
  public:
    typedef unsigned int EEPROM_Key;
    typedef struct EEPROM_Header {
      byte numChips;
      EEPROM_Key headerKey;
      EEPROM_Address chipLastAddress;
      byte recordSize;
      byte firstFreeChip;
      EEPROM_Address firstFreeByte;
    } EEPROM_Header;

    /* Constructor. TheMaintenancePeriodInSec defined the period used to update the header in EEPROM: updating it at every write
     * would use too many write cycles. A delay of 10 seconds reduces that cost significantly without any problem: the board should
     * just run a few seconds after the end of the measurements for the header to be up-to-date. 
     */
    EEPROM_BankWriter(const byte theMaintenancePeriodInSec = 10) ;
    VIRTUAL ~EEPROM_BankWriter() {};
    /* Initialize everything according to available hardware.
       Assumes hw.getNumExternalEEPROM() > 0. */
    VIRTUAL bool init(const EEPROM_Key key, const HardwareScanner &hw, const byte recordSize);
    VIRTUAL_FOR_TEST bool doIdle(bool forceImmediateMaintenance=false);

    // Information methods
    NON_VIRTUAL unsigned long getTotalSize() const;
    NON_VIRTUAL unsigned long getFreeSpace() const;
    NON_VIRTUAL inline unsigned long getNumFreeRecords() const {
      return getFreeSpace() / header.recordSize;
    }
    NON_VIRTUAL inline bool isInitialized() const {
      return flags.initialized;
    }
    NON_VIRTUAL inline bool memoryFull() const {
      return flags.memoryFull;
    }

    // Write methods
    NON_VIRTUAL bool storeOneRecord(const byte* data, const byte dataSize);
    
    /** @brief write size bytes to memory. 
     *  If the data reaches the end of the current chip, the next bytes are written in the next chip. 
     *  Writing stops if it reaches the end of the data or the end of the last chip.
     *  
     *  @param data The pointer to the first byte of the buffer to write into the EEPROM 
     *  @param dataSize The number of bytes to write (if 0 the function does not do anything).
     *  @return The number of bytes actually written (less than the requested number if the end of the data or
     *  the end of the last chip is encountered.
     */
    NON_VIRTUAL bool storeData(const byte* data, const byte size);
    
    // Completely empty the EEPROM bank (logically). If any chip is not used
    // it is used after a call to erase().
    NON_VIRTUAL void erase();
    // Empty the EEPROM bank from the byte located at addressInChip in the
    // chip numbered chipId (counted from 0, 0 being the first chip in use).
    NON_VIRTUAL void eraseFrom(byte chipId, EEPROM_Address addressInChip);

  protected:
    typedef struct Flags {
      byte initialized: 1;
      byte headerToBeUpdated: 1;
      byte memoryFull: 1;
    } Flags;

    // Methods for use by the subclasses.
    NON_VIRTUAL inline const EEPROM_Header& getHeader() const {
      return header;
    }
    NON_VIRTUAL inline const HardwareScanner* getHardware() const {
      return hardware;
    }
    /* Read data. 32 bytes is the max size allowed by underlying Wire library
       Caution: EEPROM_SequenceNumber is the chip number (from 0 to 7) not the I2C Address. */
    VIRTUAL_FOR_TEST byte readFromEEPROM(const byte EEPROM_SequenceNumber, const EEPROM_Address address, byte * buffer, const byte size) const;
    VIRTUAL_FOR_TEST byte writeToEEPROM(const byte EEPROM_SequenceNumber, const EEPROM_Address address, const  byte * buffer,  const byte size);

    NON_VIRTUAL inline byte getFirstEEPROM_Chip() const {
      return firstEEPROM_Chip;
    }
    NON_VIRTUAL inline const Flags& getFlags() const {
      return flags;
    }


  private:
    EEPROM_Header header;
    byte firstEEPROM_Chip;
    Flags flags;
    const HardwareScanner * hardware;
    elapsedMillis timeElapsedSinceMaintenance;
    byte maintenancePeriodInSec; // The period (in seconds) used for checking the need for header update.
    
};
