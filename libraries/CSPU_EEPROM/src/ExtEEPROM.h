#pragma once
#include "Arduino.h"

typedef unsigned int EEPROM_Address; // 0 to 65535. Covers the address range of 64kb EEPROM chips.

/** @ingroup CSPU_EEPROM
 *  @brief A COMPLETER
 *  
 *  DESCRIPTION A COMPLETER.
 */
class ExtEEPROM {
  public :
   
    static bool writeByte( const byte I2C_Address, const EEPROM_Address address, const byte data);
    // Returns true if write was ok.
    
    static bool readByte( const byte I2C_Address, const EEPROM_Address address, byte &data);
    
    static byte writeData( const byte I2C_Address, const EEPROM_Address startAddress, const byte * buffer, const byte bufferSize );
    // Write the content of buffer to EEPROM, avoiding cross-page writes.
    // Return the number of bytes written. 
    
    static byte readData( const byte I2C_Address, const EEPROM_Address address, byte * buffer, const byte bufferSize );
    // Returns the number of bytes read. If bufferSize > size of I2C buffer, the data is read in multiple reads.

  protected:
    static byte readSmallData(const byte I2C_Address, const EEPROM_Address address, byte * buffer, const byte bufferSize);
    // Returns the number of bytes read. Fails if bufferSize > 32.    

  
};


