#ifdef NEVER_COMPILE_THIS
/*
- Problems encountered:
    - MotionCal does not accept end of line using ENDL="\n". Use Serial.println() instead, which is probably \r\n.
    - Took some time to understand that MotionCal
        - Only converges if raw data are about 0.1µT/step (which is the raw value resolution for the NXP, not the LSM)
        - Somehow assumes this resolution and provides offsets to be applied on the values ** in µTesla **. 

*/
#endif
