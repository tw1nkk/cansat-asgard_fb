/*
 * IMU_ClientLSM9DS0.h
 */
 
#pragma once

#include "IMU_Client.h"
#include "Arduino.h"
#include "Wire.h"
#include "Adafruit_Sensor.h"
#include "Adafruit_LSM9DS0.h"
#include "IsaTwoRecord.h"

/** A class to query the LSM9DS0 Inertial Management Unit and 
 *  store the results into an IsaTwoRecord for broadcasting.
 */
class IMU_ClientLSM9DS0 : public IMU_Client
{
  public:
	static constexpr byte defaultNumSamplesPerRead=5;
    /** Constructor. Assumes LSM9DS0 is connected to the SDA and SCL pins 
     */
	IMU_ClientLSM9DS0(byte nSamplesPerRead=defaultNumSamplesPerRead);
    /** Initialize driver. Call before using readData()
     *  @return true if initialization ok.
     *  @pre Serial and Wire must be initialized
     */
    virtual bool begin();
  protected:
    /** Make a single reading of all 9 sensors, and store magnetic field in µT, gyro
     *  in raw sensor steps and rps, acceleration in raw sensor steps and m/s^2 to
     *  the current values in the record.
     *  NB: the record timestamp is NOT modified.
     *  @param record The record to update with the data.
     */
    virtual void readOneData(IsaTwoRecord& record);
  private:
    Adafruit_LSM9DS0 lsm ;
};
