#include "RT_Commander.h"

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_DIAGNOSTIC 1
#define DBG_READ_MSG 0
#define DBG_GET_PARAM 0

#include "IsaTwoInterface.h"
#include "IsaTwoHW_Scanner.h"  // For RF_OPEN/CLOSE_STRING


RT_Commander::RT_Commander(byte theCommandTypeId, unsigned long int theTimeOutInMsec):
  sd(NULL),
  commandTypeId(theCommandTypeId),
  cmdModeTimeOut(theTimeOutInMsec),
  inactivityDuration(0),
  state(State_t::Acquisition),
  RF_Stream(NULL),
  acqProcess(NULL)
{}


#ifdef RF_ACTIVATE_API_MODE
void RT_Commander::begin (IsaTwoXBeeClient& xbeeClient, SdFat* theSd, AcquisitionProcess* theProcess) {
  RF_Stream = &xbeeClient;
  sd = theSd;
  acqProcess = theProcess;
}
#else
void RT_Commander::begin (Stream& theResponseStream, SdFat* theSd, AcquisitionProcess* theProcess) {
  RF_Stream = &theResponseStream;
  sd = theSd;
  acqProcess = theProcess;
}
#endif

void RT_Commander::processMsg (const char * theMsg) {
  DASSERT(RF_Stream);
  DPRINTS(DBG_READ_MSG, "processMsg:  received ");
  DPRINTLN(DBG_READ_MSG, theMsg);



  static constexpr byte CmdBufferSize=100;
  if (strlen(theMsg) >= CmdBufferSize-1) {
	  DPRINTSLN(DBG_DIAGNOSTIC, "RT_Commander::processRequest: Command too large (ignored).");
	  return;
  }

  // Processing the request will modify it. To honor the const-ness of cmd,
  // we work on a copy.
  char cmdCopy[CmdBufferSize];
  strcpy(cmdCopy, theMsg);
  char* nextCharAddress=cmdCopy;
  // Remove final '\n' if any
  while ((cmdCopy[strlen(cmdCopy)-1] == '\n') || (cmdCopy[strlen(cmdCopy)-1] == '\r')) {
	  cmdCopy[strlen(cmdCopy)-1]='\0';
	  DPRINTS(DBG_READ_MSG, "Dropped final '\\n' or '\\r'. Msg:");
	  DPRINTLN(DBG_READ_MSG, cmdCopy);
  }

  int recordType = (int) strtol(cmdCopy, &nextCharAddress, 10);
  DPRINTS(DBG_READ_MSG, "record type: ");
  DPRINTLN(DBG_READ_MSG, recordType);
  if ((int)recordType == commandTypeId) {
    inactivityDuration = 0; // Reset timer first (some commands causes an early return from the function
    long int requestType;
    if (!getMandatoryParameter(nextCharAddress, requestType, "Missing request type")) return;
    DPRINTS(DBG_READ_MSG, "Request type: ");
    DPRINTLN(DBG_READ_MSG, requestType);
    switch (requestType) {
      case (int) IsaTwoCmdRequestType::InitiateCmdMode:
        if (state == State_t::Acquisition) {
          state = State_t::Command;
          DPRINTSLN(DBG_READ_MSG, "Switching to cmd mode");
        }
        RF_OPEN_STRING(RF_Stream);
        *RF_Stream << (int)IsaTwoRecordType::CmdResponse << ',' << (int)IsaTwoCmdResponseType::CmdModeInitiated << ",Command mode initiated" << ENDL;
        RF_CLOSE_STRING(RF_Stream);
        return;
        break;
      case (int) IsaTwoCmdRequestType::TerminateCmdMode:
        state = State_t::Acquisition;
        DPRINTSLN(DBG_READ_MSG, "Switching to acquisition mode");
        RF_OPEN_STRING(RF_Stream);
        *RF_Stream << (int)IsaTwoRecordType::CmdResponse << ',' << (int)IsaTwoCmdResponseType::CmdModeTerminated << ",Command mode terminated" << ENDL;
        RF_CLOSE_STRING(RF_Stream);
        break;

      default:
        break;
    }
    if (state == State_t::Command) {
       processRequest(requestType, nextCharAddress);
    }
    inactivityDuration = 0; // reset timer again in case the command processing was long.
  }
  checkTimeout();
  DPRINTS(DBG_READ_MSG, "processMsg over. msg= ");
  DPRINTLN(DBG_READ_MSG, theMsg);

}

bool RT_Commander::getParameter(char* &nextCharAddress, const char* &value, const char* defaultValue) {
	DPRINTS(DBG_GET_PARAM, "getParameter (char*). Received:");
	DPRINTLN(DBG_GET_PARAM, nextCharAddress);

	if (*nextCharAddress==',') nextCharAddress++; // Skip comma (if any)
	if (*nextCharAddress=='\0') {
		value=defaultValue; // just copy the pointer.
		return false;
	}

    char* separator=strchr(nextCharAddress, ',');
    if (separator) {
    	// Found a separator, there is something behind the parameter.
    	value=nextCharAddress;
    	nextCharAddress=separator+1;
    	*separator='\0';
    } else {
    	value=nextCharAddress;
    	// move nextCharAddress pointer to the final '\0'
    	nextCharAddress+=strlen(nextCharAddress);
	}
    return true;
}

bool RT_Commander::getParameter(char* &nextCharAddress, byte& value, byte defaultValue) {
	long aLong;
	bool found=getParameter(nextCharAddress, aLong, (long) defaultValue);
	// default value is set by getParameter();
	return found;
}

bool RT_Commander::getParameter(char* &nextCharAddress, long int& value, long int defaultValue) {
	DPRINTS(DBG_GET_PARAM, "getParameter (long). Received:");
	DPRINTLN(DBG_GET_PARAM, nextCharAddress);

	bool parameterFound=false;
	const char* stringValue;
	if (getParameter(nextCharAddress, stringValue))
	{
		char * tailPtr;
		DPRINTS(DBG_GET_PARAM, "Parsing long:");
		DPRINT(DBG_GET_PARAM, stringValue);
		DPRINTS(DBG_GET_PARAM, ", nextCharAddress is now:");
		DPRINTLN(DBG_GET_PARAM, nextCharAddress);
		value = strtol(stringValue, &tailPtr, 10);
		if  (tailPtr == stringValue) {
			DPRINTSLN(DBG_GET_PARAM, "tailPtr = stringValue");
			value=defaultValue; // No conversion occurred
		} else {
			parameterFound = true;
		}
	} else {
		value=defaultValue; // Parameter not present.
	}
	return parameterFound;
}

bool RT_Commander::getParameter(char* &nextCharAddress, uint32_t& value, uint32_t defaultValue) {
	DPRINTS(DBG_GET_PARAM, "getParameter (uint32_t). Received:");
	DPRINTLN(DBG_GET_PARAM, nextCharAddress);

	bool parameterFound=false;
	const char* stringValue;
	if (getParameter(nextCharAddress, stringValue))
	{
		char * tailPtr;
		DPRINTS(DBG_GET_PARAM, "Parsing uint32_t:");
		DPRINT(DBG_GET_PARAM, stringValue);
		DPRINTS(DBG_GET_PARAM, ", nextCharAddress is now:");
		DPRINTLN(DBG_GET_PARAM, nextCharAddress);
		value = strtoul(stringValue, &tailPtr, 10);
		if  (tailPtr == stringValue) {
			DPRINTSLN(DBG_GET_PARAM, "tailPtr = stringValue");
			value=defaultValue; // No conversion occurred
		} else {
			parameterFound = true;
		}
	} else {
		value=defaultValue; // Parameter not present.
	}
	return parameterFound;
}

bool RT_Commander::getMandatoryParameter(char* &nextCharAddress,const char* &value, const char* errorMsg) {
	DPRINTS(DBG_GET_PARAM, "getMandatoryParameter (char*). Received:");
	DPRINTLN(DBG_GET_PARAM, nextCharAddress);

	if (!getParameter(nextCharAddress, value)) {
		DPRINTSLN(DBG_READ_MSG, "Premature end of command");
		RF_OPEN_STRING(RF_Stream);
		*RF_Stream << (int)IsaTwoRecordType::CmdResponse << ',' << (int)IsaTwoCmdResponseType::MalformedRequest << "," << errorMsg << ENDL;
		RF_CLOSE_STRING(RF_Stream);
		return false;
	}
	return true;
}

bool RT_Commander::getMandatoryParameter(char* &nextCharAddress, byte& value, const char* errorMsg) {
	long aLong;
	bool result=getMandatoryParameter(nextCharAddress, aLong, errorMsg);
	if (result) {
		value=(byte) aLong;
	}
	return result;
}

bool RT_Commander::getMandatoryParameter(char* &nextCharAddress, long int& value, const char* errorMsg) {
	DPRINTS(DBG_GET_PARAM, "getMandatoryParameter (long). Received:");
	DPRINTLN(DBG_GET_PARAM, nextCharAddress);

	if (!getParameter(nextCharAddress, value)) {
		DPRINTSLN(DBG_READ_MSG, "Premature end of command");
		RF_OPEN_STRING(RF_Stream);
		*RF_Stream << (int)IsaTwoRecordType::CmdResponse << ',' << (int)IsaTwoCmdResponseType::MalformedRequest << "," << errorMsg << ENDL;
		RF_CLOSE_STRING(RF_Stream);
		return false;
	}
	return true;
}

void RT_Commander::checkTimeout() {
  DASSERT(RF_Stream);
  if ((state == State_t::Command) && (inactivityDuration >= cmdModeTimeOut)) {
    state = State_t::Acquisition;
    DPRINTS(DBG_READ_MSG, "timeout elapsed. Inactivity (msec)=");
    DPRINTLN(DBG_READ_MSG, inactivityDuration);
    RF_OPEN_STRING(RF_Stream);
    *RF_Stream << (int)IsaTwoRecordType::CmdResponse << ',' << (int)IsaTwoCmdResponseType::CmdModeTerminated
    		   << ",Acquisition mode resumed" << ENDL;
    RF_CLOSE_STRING(RF_Stream);
  }
}

void RT_Commander::processRequest(int requestType, char* cmd) {
	switch (requestType) {
		    case 0: // This is the value obtained if the strtol() conversion failed (not a number, no request type provided...
		      RF_OPEN_STRING(RF_Stream);
			  *RF_Stream << (int)IsaTwoRecordType::CmdResponse << ',' << (int)IsaTwoCmdResponseType::MalformedRequest
				         << ",Missing/Invalid request type";
			  RF_CLOSE_STRING(RF_Stream);
			  break;
	        case (int) IsaTwoCmdRequestType::DigitalWrite:
	          processReq_DigitalWrite(cmd);
	          break;

	        case (int) IsaTwoCmdRequestType::ListFiles:
	          processReq_ListFiles(cmd);
	          break;

	        case (int) IsaTwoCmdRequestType::GetFile:
	          processReq_GetFile(cmd);
	          break;

	        case (int) IsaTwoCmdRequestType::InitiateCmdMode:
	        case (int) IsaTwoCmdRequestType::TerminateCmdMode:
	          // Two commands already handled.
	          break;

	        case (int) IsaTwoCmdRequestType::StartCampaign:
	          processReq_StartCampaign(cmd);
	          break;

	        case (int) IsaTwoCmdRequestType::StopCampaign:
	          processReq_StopCampaign(cmd);
	          break;

	        default:
	          RF_OPEN_STRING(RF_Stream);
	          *RF_Stream << (int)IsaTwoRecordType::CmdResponse << ','
			  	  	  	 << (int)IsaTwoCmdResponseType::UnsupportedRequestType
			  	  	     << "," << requestType << ",Unknown Command." << ENDL;
	          RF_CLOSE_STRING(RF_Stream);
	} // Switch
}

void RT_Commander::processReq_StartCampaign(char* & /*nextcharAddress */) {
	if (acqProcess) {
		acqProcess->startMeasurementCampaign("Manual command:");
		RF_OPEN_STRING(RF_Stream);
		*RF_Stream << (int) (IsaTwoRecordType::CmdResponse) << ','
				<< (int) (IsaTwoCmdResponseType::CampaignStatus) << ',' << 1
				<< ",Campaign started" << ENDL;
		RF_CLOSE_STRING(RF_Stream);
	} else {
		RF_OPEN_STRING(RF_Stream);
		*RF_Stream << (int) (IsaTwoRecordType::CmdResponse) << ','
				<< (int) (IsaTwoCmdResponseType::UnsupportedRequestType) << ','
				<< (int) (IsaTwoCmdRequestType::StartCampaign) << ",No known acquisition process" << ENDL;
		RF_CLOSE_STRING(RF_Stream);
	}
}

void RT_Commander::processReq_StopCampaign(char* & /*nextcharAddress */) {
	if (acqProcess) {
		acqProcess->stopMeasurementCampaign();
		RF_OPEN_STRING(RF_Stream);
		*RF_Stream << (int) (IsaTwoRecordType::CmdResponse) << ','
				<< (int) (IsaTwoCmdResponseType::CampaignStatus) << ',' << 0
				<< ",Campaign stopped" << ENDL;
		RF_CLOSE_STRING(RF_Stream);
	} else {
		RF_OPEN_STRING(RF_Stream);
		*RF_Stream << (int) (IsaTwoRecordType::CmdResponse) << ','
				<< (int) (IsaTwoCmdResponseType::UnsupportedRequestType) << ','
				<< (int) (IsaTwoCmdRequestType::StopCampaign) << ",No known acquisition process" << ENDL;
		RF_CLOSE_STRING(RF_Stream);
	}
}

void RT_Commander::processReq_DigitalWrite(char* &nextCharAddress) {
	byte pinNbr, newState;
    if (!getMandatoryParameter(nextCharAddress, pinNbr, "Missing pin number")) return;
    DPRINTS(DBG_READ_MSG, "Digital write: pin# ");
    DPRINTLN(DBG_READ_MSG, pinNbr);
    if (!getMandatoryParameter(nextCharAddress, newState, "Missing pin state")) return;
    DPRINTS(DBG_READ_MSG, " new state =  ");
    DPRINTLN(DBG_READ_MSG, newState);
    digitalWrite(pinNbr, newState);
    RF_OPEN_STRING(RF_Stream);
    *RF_Stream << (int)IsaTwoRecordType::CmdResponse << ',' << (int)IsaTwoCmdResponseType::DigitalWriteOK << ',' << pinNbr << ','
  		     << newState << ",pin#" << pinNbr << " now " << (newState? "HIGH":"LOW") << ENDL;
	RF_CLOSE_STRING(RF_Stream);
}

void RT_Commander::processReq_ListFiles(char* &nextCharAddress) {
	const char *path;
	if (!getMandatoryParameter(nextCharAddress, path, "Missing path")) return;
	DPRINTS(DBG_READ_MSG, "List files, dir=");
	DPRINTLN(DBG_READ_MSG, path);

	File dir = sd->open(path, FILE_READ);
	if (dir.isDir()) {
		DPRINTSLN(DBG_READ_MSG, "listing Files on the SD Card");
		int counter=0;
		bool complete=true;
		while (1) {
			File entry =  dir.openNextFile();
			constexpr int FileNameLength=25;
			char fileName[FileNameLength];
			if (! entry) {
				// no more files
				break;
			}
			if (counter > 1000) {
				// Never list more than 1000 files.
				complete=false;
				break;
			}
			counter++;
			entry.getName(fileName, FileNameLength);
			RF_OPEN_STRING_PART(RF_Stream);
			*RF_Stream << (int)IsaTwoRecordType::CmdResponse << ',' << (int)IsaTwoCmdResponseType::FileListEntry
							   << ',' << counter << ',' << fileName;
			if (entry.isDirectory()) {
				*RF_Stream << '/' << ENDL;
			} else {
				// files have sizes, directories do not
				*RF_Stream << ',' << entry.size() << ENDL;
			}
			RF_CLOSE_STRING_PART(RF_Stream);
			entry.close();
			delay(100); // Avoid overflowing the radio channel

		}
		RF_OPEN_STRING(RF_Stream);
		*RF_Stream << (int)IsaTwoRecordType::CmdResponse << ',' << (int)IsaTwoCmdResponseType::FileListComplete
				   << ',' << path << ',' << counter ;
		if (complete) {
			*RF_Stream << ",1, list complete (" << counter << " entries)" << ENDL;
		}
		else {
			*RF_Stream << ",0, list truncated" << ENDL;
		}
		RF_CLOSE_STRING(RF_Stream);
	} else {
		RF_OPEN_STRING(RF_Stream);
		*RF_Stream << (int)IsaTwoRecordType::CmdResponse << ',' << (int)IsaTwoCmdResponseType::InvalidPath
				   << ',' << path <<  ",Inexistant Directory." << ENDL;
		RF_CLOSE_STRING(RF_Stream)
	}
	dir.close();
}

void RT_Commander::processReq_GetFile(char* &nextCharAddress) {

	const char* path;
	uint32_t startByte, numBytes;
	if (!getMandatoryParameter(nextCharAddress, path, "Missing path")) return;
    DPRINTS(DBG_READ_MSG, "Get file, path=");
    DPRINTLN(DBG_READ_MSG, path);
    if (getParameter(nextCharAddress, startByte, 0)) {
    	getParameter(nextCharAddress, numBytes, 1000);
    } else numBytes=1000;

	File file = sd->open(path, FILE_READ);
	if (!file) {
	   DPRINTSLN(DBG_READ_MSG, "Error Opening File");
	   RF_OPEN_STRING(RF_Stream);
	   *RF_Stream << (int)IsaTwoRecordType::CmdResponse << ',' << (int)IsaTwoCmdResponseType::InvalidPath
				   << ',' << path <<  ",Cannot open file (inexistent?)" << ENDL;
	   RF_CLOSE_STRING(RF_Stream);
		return;
	}
	if (!file.isFile()) {
		DPRINTSLN(DBG_READ_MSG, "Path is not a file");
		RF_OPEN_STRING(RF_Stream);
		*RF_Stream 	<< (int)IsaTwoRecordType::CmdResponse << ',' << (int)IsaTwoCmdResponseType::InvalidPath
					<< ',' << path <<  ",Not a regular file" << ENDL;
		RF_CLOSE_STRING(RF_Stream);
		file.close();
		return;
	}

#ifdef RF_ACTIVATE_API_MODE
	// This version is more efficient, but does not work in transparent mode:
	// Blocks including an end-of-line are not received.
	constexpr byte ReadBufferSize=MAX_FRAME_DATA_SIZE-15	; // File is sliced in blocks of n chars (Typical: 150)
	constexpr unsigned long DelayBetweenBlocks= 30;
							// Xbee in API mode: do not exceed frame size -10 bytes:
							// 20 is not enough,
						    // 30 is enough.
#else
	constexpr byte ReadBufferSize=150; // File is sliced in blocks of n chars (Typical: 150)
	constexpr unsigned long DelayBetweenBlocks= 30;
								// Xbee in Transparent mode, with buffer of 150 chars:
								// 20 is not enough,
							    // 30 is enough.
#endif

	DPRINTS(DBG_READ_MSG, "Processing request for file: ");
	DPRINTLN(DBG_READ_MSG, path);
	if (!file.seek(startByte)) {
		// Cannot find start position
		RF_OPEN_STRING(RF_Stream);
		*RF_Stream << (int)IsaTwoRecordType::CmdResponse << ',' << (int)IsaTwoCmdResponseType::MalformedRequest
				<< ',' << path <<  ",Cannot find position " << startByte << " in file" << ENDL;
		file.close();
		RF_CLOSE_STRING(RF_Stream);
		return;
	}
	RF_OPEN_STRING(RF_Stream);
	*RF_Stream << (int)IsaTwoRecordType::CmdResponse << ',' << (int)IsaTwoCmdResponseType::FileContent
			   << ',' << path <<",content follows (" << ReadBufferSize << ")" << ENDL;
	RF_CLOSE_STRING(RF_Stream);


	bool error=false;
	unsigned long counter=0;
	DPRINTS(DBG_READ_MSG, "Sending in blocks of ");
	DPRINT(DBG_READ_MSG, ReadBufferSize);
	DPRINTSLN(DBG_READ_MSG, " bytes");

#ifdef RF_ACTIVATE_API_MODE
	// This version is more efficient, but does not work in transparent mode:
	// Blocks including an end-of-line are not received.
	uint8_t buffer[ReadBufferSize];
	buffer[0]=(uint8_t) IsaTwoRecordType::StringPart;
	buffer[1]=' ';
	memset(buffer+2, (int) '*', NumFileContentMarkers );
	buffer[NumFileContentMarkers+2]='\0';
	RF_Stream->send(buffer, (uint8_t) NumFileContentMarkers+3);
	for (int i = 0; i< NumFileContentMarkers;i++) *RF_Stream << StartFileContentMarker;
	*RF_Stream << ENDL;
	delay(50);


	while (file.available() && (counter < numBytes)) {
		int read=file.read(buffer+2, ReadBufferSize-3);
		if (read < 0) {
			error = true;
			break;
		}
		if (counter+read >= numBytes) {
			read=numBytes-counter;
		}
		counter+=read;
		buffer[read+2]='\0';
		RF_Stream->send(buffer, read+3);
		delay(DelayBetweenBlocks);
	}
	/* memset(buffer+2, (int) '*', NumFileContentMarkers );
	buffer[NumFileContentMarkers+2]='\0';
	buffer[0]=(uint8_t) IsaTwoRecordType::StatusMsg;
	RF_Stream->send(buffer, (uint8_t) NumFileContentMarkers+3);*/
#else
	for (int i = 0; i< NumFileContentMarkers;i++) *RF_Stream << StartFileContentMarker;
	*RF_Stream << ENDL;
	delay(50);
	int  blockCounter=0;
	bool sendBlock=false;
	bool addEndLine;
	char buffer[ReadBufferSize];
	elapsedMillis timeSinceLastEmission=10000;
	while (	file.available() && (counter < (unsigned long) numBytes))
	{
		int i=file.read();
		if (i < 0) {
			error = true;
			break;
		} else {
			char c = (char) i;
			if (c=='\r') {
				// Just ignore: EOL is '\n\r'. Only react on \n.
				continue;
			}
			if ((c == '\n') || (c=='\0') ) {
				buffer[blockCounter]='\0';
				sendBlock=true;
				addEndLine= (c == '\n');
			} else {
				buffer[blockCounter]=c;
			}
		}
		if (blockCounter == (ReadBufferSize-2)) {
			buffer[blockCounter+1] = '\0';
			sendBlock=true;
			addEndLine=false;
		}
		counter++;
		blockCounter++;
		if (sendBlock) {
			while (timeSinceLastEmission < DelayBetweenBlocks) {
				delay(10);
			}
			// NB: In transparent mode, nothing is sent before an ENDL is sent!
			*RF_Stream  << counter << "*, " << (const char*) buffer;
			   // Removing the counter and "*," causes the transmission to fail after every
			   // Newline found in the file.  WHY ??
			   // Use a comma to allow for easy removal of the column.
			if (addEndLine) {
				*RF_Stream << ENDL;
				timeSinceLastEmission=0;
			}
			blockCounter=0;
			sendBlock=false;
		}
	}
#endif

	if (error) {
		RF_OPEN_STRING(RF_Stream);
		*RF_Stream << ENDL;
		*RF_Stream << (int)IsaTwoRecordType::CmdResponse << ','
				<< (int)IsaTwoCmdResponseType::FileError
				<< ",Error reading file" << ENDL;
		RF_CLOSE_STRING(RF_Stream);
	} else {
		RF_OPEN_STRING(RF_Stream);
		*RF_Stream << ENDL;
		for (int i = 0; i< NumFileContentMarkers;i++) *RF_Stream << StartFileContentMarker;
		*RF_Stream << ENDL;
		RF_CLOSE_STRING(RF_Stream);
		RF_OPEN_STRING(RF_Stream);
		*RF_Stream << (int)IsaTwoRecordType::CmdResponse << ','
				<< (int)IsaTwoCmdResponseType::FileContent  << ','
				<< path << ',' << startByte  << ','
				<< numBytes << (file.available() ? ",0, EOF not reached" : ",1, EOF reached")
				<< ENDL;
		RF_CLOSE_STRING(RF_Stream);
	}
	file.close();
}

