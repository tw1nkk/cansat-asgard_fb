/*
    test_ClockSynchronizer/SlaveBoard.ino

    See documentation in MasterBoard.ino.
*/

#define CHECK_RESYNC //Define to have the slave cancel synchronization flag every 10 sec (debugging only).
#define DEBUG_CSPU
#include "DebugCSPU.h"

#include "ClockSynchronizer.h"
#include "Wire.h"

constexpr byte SlaveAddress = 0xBB;
unsigned long ts;

void setup() {
  DINIT(115200);
  Wire.begin(SlaveAddress);
  
  ClockSynchronizer_Slave::begin();
  Serial << "Slave registered ; waiting for 10 secs to receive sync..." << ENDL;
  Serial.flush();
  ts = millis();
  while ( ((millis()- ts)< 1000000) && (!ClockSynchronizer_Slave::isSynchronized())) {
    delay(10);
  }
  Serial << "Synchronized=" << ClockSynchronizer_Slave::isSynchronized() << ENDL;
  ts = millis();
}

void loop() {
  unsigned long masterClock;
  bool result = ClockSynchronizer_Slave::getMasterClock(masterClock);
  if (!result) {
    Serial << " **** getMasterClock() return FALSE" << ENDL;
  }
  // Serial << "MasterAdvance=" << ClockSynchronizer_Slave::getMasterAdvance() << ENDL;
  Serial << "Master clock on slave: " << masterClock << ENDL;

#if (defined(DBG_IRQ) && defined(CHECK_RESYNC))
  if ((millis() - ts) > 10000) {
    Serial << "cancelling sync: getMasterClock should return false until next sync by master" << ENDL;
    ClockSynchronizer_Slave::syncReceived = false;
    ts=millis();
  }
#endif
  delay(200);
}
