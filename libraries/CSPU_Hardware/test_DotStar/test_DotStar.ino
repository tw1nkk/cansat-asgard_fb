/*  Test program for DotStar class. bitbanging the RGB LED (to shut it down). 
*/
#ifndef ARDUINO_ITSYBITSY_M4
#error "Test currently only supports ItsyBitsy M4"
#endif

#define CSPU_DEBUG
#include "DebugCSPU.h"
#include "CSPU_Test.h"
#include "DotStar.h"

void setup() {
  DINIT(115200);


  Serial << "ready to shutdown on-board DotStar..." ;
  CSPU_Test::pressAnyKey();

  Serial << "Turning off RGB LED..." << ENDL;
  DotStar::shutdownOnBoard();
  
  Serial << "DotStar now shut down" << ENDL;
  Serial << "End of job " << ENDL;
  exit(0);
}

void loop() {
  // put your main code here, to run repeatedly:

}
