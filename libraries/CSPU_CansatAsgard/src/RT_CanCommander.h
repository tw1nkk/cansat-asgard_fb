#pragma once
#include "Arduino.h"
#include "CansatAcquisitionProcess.h"
#include "elapsedMillis.h"
#include "CansatConfig.h"
#include "CansatInterface.h"
#ifdef RF_ACTIVATE_API_MODE
#include "CansatXBeeClient.h"
#endif

/** @ingroup CSPU_CansatAsgard
    @brief This class is in charge of the Real-time communication with the ground.

    It receives commands and sends responses.
    Commands must be in CSV format, as described in CansatInterface.h

    The class processes strings received with type CansatFrameType::CmdRequest
    (but without the CansatFrameType field).

    The first field indicates the command's type, which can be one of the
    CansatCmdRequest enum values (see CansatInterface.h) or a project specific
    value processed by a project-specific subclass of the RT_CanCommander.
    Next fields are the request parameters, as specified in CansatInterface.h.

    The RT_CanCommander manages the two operation modes:
      - the command mode: all request are processed.
      - the acquisition mode: only request CansatCmdRequest::InitiateCmdMode is
       	   processed (others are ignored)

    The command mode reverts automatically to acquisition mode after a timeout.

    Projects requiring specific commands in addition to the ones defined in
    CansatInterface.h must subclass the RT_CanCommander and override function
    processProjectCommand. Implementation should make use of the various
    variants of the getParameter() method in order to parse the command.

    NB: Although normally possible, the of XBees in transparent mode has not been tested.

    Historical note: this class was originally developed for the IsaTwo project, and
    later moved to CansatAsgardCSPU for reuse.

    @see CansatInterface.h
*/


class RT_CanCommander {
  public:
    /** @brief The various states the can be be in. */
    enum class State_t {
      Acquisition,                /**< Acquisition mode state: the RT-Commander does not respond to commands except to switch to Command mode */
      Command                     /**< Command mode state: the RT-Commander is responding to commands. The RT-Commander switches back to Acquisition mode after a predefined timeout */
    };

    /** @brief Constructor of the class RT_Commander.
        @param theTimeOut The duration (in milliseconds) for the Command mode to time out (and switch back to acquisition).
    */
    RT_CanCommander(unsigned long int theTimeOut);
    virtual ~RT_CanCommander() {};

#ifdef RF_ACTIVATE_API_MODE
    /** @brief Method used to initialize the object when using the RF API mode.
         @param xbeeClient The interface to the XBee module to used for output .
         @param theSd Initialized object of type SdFat that will be used by the class for various Sd Card related actions.
         @param theProcess Initialized object of type CansatAcquisitionProcess used for interaction with it by the class when handling commands.
     */
     void begin(CansatXBeeClient& xbeeClient, SdFat* theSd = NULL, CansatAcquisitionProcess* theProcess = NULL);
#else
    /** @brief Method used to initialize various pointers.
        @param RF_Stream The output stream used by the class.
        @param theSd Initialized object of type SdFat that will be used by the class for various Sd Card related actions.
        @param theProcess Initialized object of type CansatAcquisitionProcess used for interaction with it by the class when handling commands.
    */
    void begin(Stream& RF_Stream, SdFat* theSd = NULL, CansatAcquisitionProcess* theProcess = NULL);
#endif

    /** @brief Method used to process a command.
        @param theRequest The string to analyze and possibly process. It is assumed that the caller
        		checked that it is of type CansatFrameType::CmdRequest. The string does NOT
        		include that type information (first field is the request type).
    */
    void processCmdRequest(const char* theRequest);

    /** @brief Method used to check if the Command mode has timed out.*/
    State_t currentState() {  checkTimeout();   return state; 	}

  protected:
    // ------------- THE NEXT FUNCTION SHOULD BE OVERRIDDEN BY SUBCLASSES ---------------
    /** @name Methods that should be overridden by subclasses
     *  @{
     */
    /** Process a project specific command request while in command mode.
     *  This method should be overriden by any project-specific subclass
     *  to add non standard commands. This method should not report an error if
     *  the CmdRequest received is not recognized (just return false) but should
     *  report errors if the request is recognized but syntactically incorrect.
     * @param requestType the request type value
     * @param cmd	Pointer to the first character after the command type. Command parameter can be parsed from
     * 				this position which should point to a separator if any parameter is present, or to the final '\0'
     * 				if none is provided.
     * @return True if the command was processed, false otherwise.
     */
     virtual bool processProjectCommand(CansatCmdRequestType /* requestType */, char* /* cmd */) {return false;};

     /** Perform any action required before the can is powered down
      * @return True if everything went as planned.
      */
     virtual bool preparePrimaryMissionForShutdown() {return true;};

     /** Cancel any action performed to prepare the can for powering down,
      *  so normal operation can be resumed.
      * @return True if everything went as planned.
      */
     virtual bool cancelPrimaryMissionShutdown() {return true;};
    /// @}

     // --------- THE NEXT FUNCTIONS SHOULD NOT BE OVERRIDDEN BY SUBCLASSES --------------
     /** @name Methods that should normally not be overridden by subclasses
      * @{
      */
    /** Process a request while in command mode.
     * @param requestType the request type value
     * @param cmd	Pointer to the first character after the command type. Command parameter can be parsed from
     * 				this position which should point to a separator if any parameter is present, or to the final '\0'
     * 				if none is provided.
     * @return True if the command was processed, false otherwise.
     */
    bool processStandardCommand(CansatCmdRequestType requestType, char* cmd);
    void processReq_DigitalWrite(char* &nextCharAddress);
	void processReq_ListFiles(char* &nextCharAddress);
	void processReq_GetFile(char* &nextCharAddress);
	void processReq_GetCampaignStatus(char* &nextCharAddress);
	void processReq_StartCampaign(char* &nextCharAddress);
	void processReq_StopCampaign(char* &nextCharAddress);
	void processReq_PrepareShutdown(char* &nextCharAddress);
	void processReq_CancelShutdown(char* &nextCharAddress);

	/** Extract a mandatory integer parameter from a command.
	 *  @param nextCharAddress Pointer to the first character to parse. Should point to a separator if any parameter is present,
	 *  					   or to the final '\0'	if none is provided.
	 *  					   After parsing, this pointer is updated to the first unparsed character.
	 *  @param value The parsed value
	 *  @param errorMsg The error message to use if the required parameter cannot be found.
	 *  @return True if the parameter was successfully parsed, false otherwise.
	 */
	bool getMandatoryParameter(char* &nextCharAddress, long int& value, const char* errorMsg);
	bool getMandatoryParameter(char* &nextCharAddress, byte& value, const char* errorMsg);
	bool getMandatoryParameter(char* &nextCharAddress, const char* &value, const char* errorMsg);

	/** Extract an optional integer parameter from a command.
	 *  @param nextCharAddress Pointer to the first character to parse. Should point to a separator if any parameter is present,
	 *  					   or to the final '\0'	if none is provided.
	 *  					   After parsing, this pointer is updated to the first unparsed character.
	 *  @param value The parsed value
	 *  @param defaultValue The value to use in case the parameter is not present or cannot
	 *  					be parsed.
	 *  @return True if the parameter was found and successfully parsed, false if the
	 *  			 it was absent or invalid and the default value was used.
	 */
	bool getParameter(char* &nextCharAddress, byte& value, byte defaultValue=0);
	bool getParameter(char* &nextCharAddress, long int& value, long int defaultValue=0L);
	bool getParameter(char* &nextCharAddress, uint32_t& value, uint32_t defaultValue);

	/** Extract an optional string parameter from a command.
	 *  @param nextCharAddress Pointer to the first character to parse. Should point to a separator if any parameter is present,
	 *  					   or to the final '\0'	if none is provided.
	 *  					   After parsing, this pointer is updated to the first unparsed character.
	 *  @param value (out) A pointer to the parsed value, or a pointer to the default value, if no
	 *  				   value provided in the command.
	 *  @param defaultValue The value to use in case the parameter is not present or cannot
	 *  					be parsed.
	 *  @return True if the parameter was found and successfully parsed, false if the
	 *  			 it was absent or invalid and the default value was used.
	 */
	bool getParameter(char* &nextCharAddress, const char*& value, const char* defaultValue="");

    /** @brief Method used to check the time out to switch to Acquisition mode.*/
    void checkTimeout();

    /** Check the availability of the SdFat interface to the SD-Card. If not available,
     *  a "CommandNotSupported" response is sent.
     *  @return True if available, false otherwise.
     */
    bool checkSdAvailable(CansatCmdRequestType cmd) const;

    /** Perform any action required before the can is powered down
     *  This method delegates the work to the SecondaryMissionController, if any
     * @return True if everything went as planned.
     */
    virtual bool prepareSecondaryMissionForShutdown() {
    	if (acqProcess->getSecondaryMissionController()) {
    		return acqProcess->getSecondaryMissionController()->prepareSecondaryMissionForShutdown();
    	}
    	else return true;
    };

    /** Cancel any action performed to prepare the can for powering down,
     *  so normal operation can be resumed.
     *  This method delegates the work to the SecondaryMissionController, if any
     * @return True if everything went as planned.
     */
    virtual bool cancelSecondaryMissionShutdown() {
    	if (acqProcess->getSecondaryMissionController()) {
    		return acqProcess->getSecondaryMissionController()->cancelSecondaryMissionShutdown();
    	}
    	else return true;
    };
    ///@}

#ifdef RF_ACTIVATE_API_MODE
    CansatXBeeClient* RF_Stream;			  /**< The pseudo stream used when the API mode is activated. Available for subclasses. */
#else
    Stream* RF_Stream; 	                      /**< The stream used to output command responses (when using
     	 	 	 	 	 	 	 	 	 	 	   transparent mode (available for subclasses) */
#endif

  private:
    SdFat* sd;                                /**< The sd object used for various sd related actions.*/
    unsigned long int cmdModeTimeOut; 	      /**< The time before switching to acquisition mode.*/
    elapsedMillis inactivityDuration; 		  /**< Time spent in command-mode without activity.*/
    State_t state; 				              /**< The current state of the RT_Commander.*/
    CansatAcquisitionProcess* acqProcess;           /**< A pointer to the acquisition process, to interact with it, when handling commands.*/
};
