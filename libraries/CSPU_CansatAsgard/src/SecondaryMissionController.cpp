/*
 * SecondaryMissionController.cpp
 */

#include "CansatConfig.h"
#include "SecondaryMissionController.h"

 void SecondaryMissionController::run(CansatRecord& record) {
	if (elapsed >= CansatSecondaryMissionPeriod) {
		elapsed=0;
		manageSecondaryMission(record);
	}
}

bool SecondaryMissionController::begin(CansatXBeeClient* xbeeClient) {
	 xbee=xbeeClient;
	 return true;
};


