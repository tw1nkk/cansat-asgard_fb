/* 
 *  Test program for ReferenceAltitude feature in the BMP_Client class
 *
 *  It does not require a BMP to be connected.
 */
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"
#include "BMP_Client.h"


#ifndef INCLUDE_REFERENCE_ALTITUDE
#error "The symbol INCLUDE_REFERENCE_ALTITUDE should be defined to enable the reference altitude feature"
#endif

bool stopAfterFailedTest=false;
bool stopAfterFirstFailedCheck=false;
uint32_t numErrors;
uint32_t recordCounter;
CansatRecord record;
BMP_Client bmp;


class RefAltitude_Test {

public:
	static void printTestSummary() {
		Serial << ENDL << "===== Test over (processed " << recordCounter << " records). Errors so far: "
				<< numErrors  << " =====" << ENDL;
		if (numErrors && stopAfterFailedTest) {
			Serial << "Stopping because of 'stopAfterFailedTest' setting in test program" << ENDL;
			delay(500);
			exit(-1);
		}
	};
	static void RunA_RecordForRefAltitudeTest(CansatRecord& record, float altitude) {
		record.altitude=altitude+(random(0,150)-75.0f)/100.0f;
		record.timestamp+=CansatAcquisitionPeriod;
		bmp.updateReferenceAltitude(record);
		recordCounter++;
		if ((recordCounter % 100) == 0) {
			Serial << ".";
		}
	};

	static void TestReferenceAltitudeCalculation() {
		recordCounter=0;
		randomSeed(analogRead(0));

		record.clear();
		uint32_t start=100000;
		float ground=500;
		record.timestamp=start;
		Serial << ENDL << "===== Testing reference altitude calculation ===== " << ENDL;

		// 0. Allow for a first set of the reference altitude.
		Serial <<  ENDL << "0. Processing enough records to initialize reference altitude..." << ENDL;
		while ((record.timestamp -start)< 1.5*NoAltitudeChangeDurationForReset) {
			RunA_RecordForRefAltitudeTest(record, ground);
		}
		// 1. Simulate situation before take-off:  random changes [x-0,75:x+0,75]
		//    with periods of slow change when can is moved.
		Serial << ENDL << "1. Simulating can being moved on the ground..." << ENDL;
		for (int k=0 ; k < 5; k++) {
			// 1a no change except for noise.
			Serial <<  ENDL << "Cycle " << k+1 << " of 5. Ground altitude is " << ground << "m" << ENDL;
			start=record.timestamp;
			uint32_t duration=3.1*NoAltitudeChangeDurationForReset;
			Serial << "Not moving except noise for " << duration/1000 << " seconds..." << ENDL;
			while ((record.timestamp -start)< duration) {
				RunA_RecordForRefAltitudeTest(record, ground);
				if (fabs(record.refAltitude - ground) > 0.3) {
					Serial << "*** Error: refAltitude=" << record.refAltitude << " while ground=" << ground << ENDL;
					numErrors++;
				}
			}
			float currentRefAltitude=record.refAltitude;
			Serial <<  ENDL << "Moving slowly..." << ENDL;
			// 1b Move at 1.5 m/s during about 10 s
			// at this rate, ref altitude should not change.
			for (unsigned int i =0; i< (10000 / CansatAcquisitionPeriod); i++) {
				ground+= 1.5*(CansatAcquisitionPeriod/1000.0);
				RunA_RecordForRefAltitudeTest(record,ground);
				if (fabs(record.refAltitude - currentRefAltitude) > 0.0001) {
					Serial << "*** Error: refAltitude changed to " << record.refAltitude << " while moving at 1.5 m/s" << ENDL;
					numErrors++;
				}
			} // for i
			start=record.timestamp;
			Serial << ENDL << "Not moving to allow for ref. altitude to be updated... ground at " << ground << "m" << ENDL;
			// 1c do not move during enough time for ref altitude to be updated.
			while ((record.timestamp -start)< 1.1*NoAltitudeChangeDurationForReset) {
				RunA_RecordForRefAltitudeTest(record,ground);
			}
		} // for k

		// 2. Simulate take-off: ascent at 20 m/s up to 1000 m
		Serial << ENDL << "2. Taking off from " << ground << "m ..." << ENDL;
		float currentRefAltitude=record.refAltitude;
		float altitude=ground;
		while (altitude < (ground+1000.0)) {
			altitude+=20*(CansatAcquisitionPeriod/1000.0);
			RunA_RecordForRefAltitudeTest(record,altitude);
			if (fabs(record.refAltitude - currentRefAltitude) > 0.3) {
				Serial << "*** Error: refAltitude=" << record.refAltitude << " while ground=" << ground << ENDL;
				numErrors++;
			}
		}

		Serial << ENDL << "3. Now at " << altitude << "m... Ejecting...." << ENDL;
		// 3. Simulate ejection: descent velocity increased up to 10 m/s at
		//    10 m/s^2 + noise +-1 m/s
		float velocity=0;
		uint32_t ejectionTs=record.timestamp;
		while (velocity < 10.0) {
			velocity+= 10*(CansatAcquisitionPeriod/1000.0);
			velocity+= (random(0,200)/100.0 - 1.0);
			altitude-=velocity*(CansatAcquisitionPeriod/1000.0);
			RunA_RecordForRefAltitudeTest(record,altitude);
			if (fabs(record.refAltitude - currentRefAltitude) > 0.3) {
				Serial << "*** Error: refAltitude=" << record.refAltitude << " while ground=" << ground << ENDL;
				numErrors++;
			}
		}

		Serial << ENDL << "4. Descent velocity now " << velocity << " m/s, altitude=" << altitude << ".... descending..." << ENDL;
		// 4. Simulate descent
		while (altitude > ground) {
			altitude-=velocity*(CansatAcquisitionPeriod/1000.0);
			RunA_RecordForRefAltitudeTest(record,altitude);
			if (fabs(record.refAltitude - currentRefAltitude) > 0.3) {
				Serial << "*** Error: refAltitude=" << record.refAltitude << " while ground=" << ground << ENDL;
				numErrors++;
			}
		}
		Serial << ENDL << "Can landed after descending during " << (record.timestamp-ejectionTs)/1000.0 << "sec." << ENDL;
		Serial << "altitude=" << altitude << ", ground=" << ground << ", ref.altitude=" << record.refAltitude << ENDL;

		printTestSummary();
	};

	static void TestReferenceAltitudeCalculationWithCmdMode() {
		Serial << ENDL << ENDL;
		Serial << "==== Checking effect of CommandMode interruption on ref. altitude calculation =====" << ENDL;
		record.clear();
		uint32_t start=100000;
		float ground=500;
		record.timestamp=start;
		// 0. Allow for a first set of the reference altitude.
		Serial <<  ENDL << "0. Processing enough records to initialize reference altitude..." << ENDL;
		while ((record.timestamp -start)< 1.5*NoAltitudeChangeDurationForReset) {
			RunA_RecordForRefAltitudeTest(record,ground);
		}

		Serial << ENDL << "1. Simulating 30 seconds command mode phases interrupting acquisition while can does not move" << ENDL;
		Serial << "  Ground at " << ground << "m" << ENDL;
		float currentRefAltitude=record.refAltitude;
		for (int i = 0; i < 5 ; i++) {
			record.timestamp+=30000;
			start = record.timestamp;
			while ((record.timestamp -start)< 1.5*NoAltitudeChangeDurationForReset) {
				RunA_RecordForRefAltitudeTest(record,ground);
				if (fabs(record.refAltitude - currentRefAltitude) > 0.3) {
					Serial << "*** Error: refAltitude=" << record.refAltitude << " while ground=" << ground << ENDL;
					numErrors++;
				}
			}
		}

		Serial << ENDL << "2. Simulating 30 seconds command mode phases interrupting acquisition while can does move" << ENDL;
		Serial << "  Ground at " << ground << "m, moving slowly (1.5 m/s during about 10 s during interruption, i.e. 15 m shift)" << ENDL;
		for (int i = 0; i < 5 ; i++) {
			record.timestamp+=30000;
			ground+=15;
			start=record.timestamp;
			while ((record.timestamp -start)< 1.5*NoAltitudeChangeDurationForReset) {
				RunA_RecordForRefAltitudeTest(record,ground);
			}
			if (fabs(record.refAltitude - ground) > 0.3) {
				Serial << "*** Error: refAltitude=" << record.refAltitude << " while ground=" << ground << ENDL;
				numErrors++;
			}
		}

		Serial << ENDL
				<< "3. Simulating a problematic timing: " << ENDL;
		Serial << "   - cause a reset of the interval with an large altitude change just before" << ENDL;
		Serial << "     interruption, " << ENDL;
		Serial << "   - first record with same altitude after interruption longer than" << ENDL;
		Serial << "     the monitored delay." << ENDL;
		Serial << "   It should NOT cause a change in ref. altitude, since altitude was not continuously observed." << ENDL;
		ground+=10;
		RunA_RecordForRefAltitudeTest(record,ground);
		currentRefAltitude=record.refAltitude;
		record.timestamp+=1.5*NoAltitudeChangeDurationForReset;
		RunA_RecordForRefAltitudeTest(record,ground);
		if (fabs(record.refAltitude - currentRefAltitude) > 0.3) {
			Serial << "*** Error: ref altitude should not have been altered. " << ENDL;
			Serial << "           refAltitude=" << record.refAltitude << " previous value=" << currentRefAltitude << ENDL;
			numErrors++;
		}

		printTestSummary();
	};
}; // class RefAltitude_Test

void setup() {
	DINIT(115200);
	Serial << "Init OK" << ENDL;
	Serial << "  Acquisition period (from CansatConfig.h): " << CansatAcquisitionPeriod << " msec" << ENDL;
	Serial << "This test assumes the following settings:" << ENDL;
	Serial << "  NoAltitudeChangeDurationForReset=1 min";
	if (NoAltitudeChangeDurationForReset == 60000) {
		Serial << " OK" << ENDL;
	} else {
		Serial << ENDL << " *** Current value=" << NoAltitudeChangeDurationForReset << " msec";
		Serial << ": test data must be adapted!" << ENDL;
	}
	Serial << "  NoAltitudeChangeTolerance=2m";
	if ((NoAltitudeChangeTolerance - 2.0) < 0.001) {
		Serial << " OK" << ENDL;
	} else {
		Serial << ENDL << " *** Current value=" << NoAltitudeChangeTolerance ;
		Serial << ": test data must be adapted!" << ENDL;
	}

	// Testing referenceAltitude.
	RefAltitude_Test::TestReferenceAltitudeCalculation();
	RefAltitude_Test::TestReferenceAltitudeCalculationWithCmdMode();

	Serial << ENDL << "End of job. Number of errors: " << numErrors << ENDL;
}

void loop() {
	// put your main code here, to run repeatedly
}
