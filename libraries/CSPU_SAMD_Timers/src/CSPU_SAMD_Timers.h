/*
 * CSPU_SAMD_Timers.h
 * 
 * This files is only used for documentation of the library using Doxygen.
 * Every class in the library should include a tag @ingroup CSPU_SAMD_Timers
 * in the class documentation block.
 */

 /** @defgroup CSPU_SAMD_Timers CSPU_SAMD_Timers library
 *  @brief A couple of classes to manage portable timers (SAMD21/SAMD51)
 *  
 *  _Dependencies_\n
 *  This library requires the following other generic libraries (in addition to hardware-specific libraries,
 *  and standard Arduino libraries, which are not listed here):
 *  - CSPU_DEBUG
 *  
 *  _History_\n
 *  The library was created in Sept. 2021 to support ItsyBitsy M4 usage.
 */

