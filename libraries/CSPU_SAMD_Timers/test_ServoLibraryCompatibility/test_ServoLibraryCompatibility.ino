/*
   Test compatibility of SAMD_InterruptTimer library with Arduino Servo library (conflicts on timer allocation).
*/

#ifndef ARDUINO_ARCH_SAMD
#error "This test is not relevant for non-SAMD architectures"
#endif
#ifdef __SAMD51__
#error "This test is not relevant for SAMD51 architecture, since it is not supported by the Servo library"
#endif
// Architecture is SAMD21...

// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop


#include "CansatConfig.h"
#include "SAMD_InterruptTimer.h"
#include "Servo.h"

Servo myServo;
SAMD_InterruptTimer timer;
void dummyHandler() {
  Serial << "Timer expired" << ENDL; 
  };

void setup() {
  Serial.begin(115200);
  while (!Serial);
  Serial << "Making use of one SAMD_InterruptTimer feature to make sure it is linked" << ENDL;
  Serial << "Available timers: " << SAMD_InterruptTimer::getNumAvailableTimers() << ENDL;
  Serial << "Starting time to make sure the HW timer is configured" << ENDL;
  timer.start(1000, dummyHandler);

  Serial << "Making use of a Servo feature to make sure it is linked" << ENDL;
  myServo.attach(9);
  myServo.write(0);

  delay(5000);
  timer.clear();
  Serial << "End of job." << ENDL;
} // setup

void loop() {
  delay(1000);
}
