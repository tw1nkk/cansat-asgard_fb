/*
   XBeeClient.h
*/

#pragma once
#include "CansatConfig.h" // for RF_ACTIVATE_API_MODE
#include "XBeeTypes.h"

#ifdef RF_ACTIVATE_API_MODE

#include "Arduino.h"
#include <XBee.h>
#include "StringStream.h"
#ifdef ARDUINO_AVR_UNO
#  include "SoftwareSerial.h"
#endif

/**
 * @ingroup CSPU_XBee
 * @brief Interface class to transport data between XBee modules in API mode, and
 * interact with the XBee module with (local and remote) AT commands.
 * @par Data transmission
 * XBeeClient allows for sending and receiving binary payloads (methods send and receive)
 * and for sending strings (methods openStringMsg, operator << and closeStringMsg.
 * Strings are transported as binary payloads including:
 * 	- A type identifier (1 byte)
 * 	- 0xFF as second byte.
 * 	- The ascii codes of the characters.
 * 	- A final \ 0 delimiter
 * Detecting string messages among received payloads are the user responsibility.
 *
 * @par XBee module local and remote management
 * XBeeClient also provides a number of utility methods to issue AT Commands,query
 * the value of configuration parameters, display a configuration summary etc.
 * Subclasses should implement method doCheckXBeeModuleConfiguration() to actually
 * perform relevant checks using the various checkXXX methods.
 *
 * @par Debugging:
 * In addition to usual debugging switches in the cpp files, incoming and/or outgoing
 * frames are displayed on Serial if protected constants displayOutgoingFramesOnSerial
 * and/or displayIncomingFramesOnSerial are true
 *
 */
class XBeeClient {
  public:
    /** Constructor The default destination address is used when no destination address is
      	  	  	    provided to the send() method.
      @param defaultDestinationSH The Serial Number High specific of the default destination address.
      @param defaultDestinationSL The Serial Number Low specific of the default destination address.
    */
    XBeeClient(uint32_t defaultDestinationSH, uint32_t defaultDestinationSL);
    virtual ~XBeeClient() {};

    /** Initialize point to point radio link
        @param stream The serial object to use to communicate with
                the XBee module if a HardwareSerial is used.
                This object is expected to be active and configured with the baudrate
    			used by the XBee module.
    */
    virtual void begin(HardwareSerial &stream);

#ifdef ARDUINO_AVR_UNO
    /** Initialize point to point radio link
        @param stream The serial object to use to communicate with
                the XBee module if a SoftwareSerial is used
                This object is expected to be active and configured with the baudrate
    			used by the XBee module.
     */
    virtual void begin(SoftwareSerial &stream);
#endif

    /** @name Transmitting data
     *  Those functions are used to transmit data through a pair of XBee module.
     */
    ///@{
    /** Send data (after begin() has been called)
        @param data The payload to transfer
        @param dataSize The number of bytes of the payload
        @param timeOutCheckResponse The maximum delay to wait for a reception
        	   	   	   	   	   	    acknowledgment by the destination XBee (msec). If 0,
        							the reception ack will not be checked.
        @param destSH The destination address (high word)
        @param destSL The destination address (low word).  If destSH=destSL=0,
        	          the default address provided in the constructor is used.
        @return True if transfer successful, false otherwise.
    */
    virtual bool send(uint8_t* data,
                      uint8_t dataSize,
                      int timeOutCheckResponse = 300,
                      uint32_t destSH = 0,
                      uint32_t destSL = 0);

   /** Check whether data is received, the user should then print data using a for{} loop
    *  (after begin() has been called).
    *  If the XBee module is sleeping, this method always return false. It is the
    *  callers responsibility to wake up the XBee module if required (use the
    *  moduleInSleepMode(), wakeUpXBeeModule() and putXBeeModuleToSleep()
    *  methods
    *  @param payload If anything is received, this pointer is set
    *           to an internal buffer containing the received
    *           payload. Ownership of this buffer remains in the object.
    *  @param payloadSize If anything is received, this parameters is
    *           set to the number of bytes of the received payload.
    *  @return True if anything received, false otherwise
    */
    virtual bool receive(uint8_t* &payload, uint8_t& payloadSize);

    /** Start a string message. This method should be called before streaming the
     *  string to be transported using operator<< (and after begin() has been called).
     *  @param type A type identifier, transported as first byte of the payload.
     *  			(string will start at byte 3.
     *  @param seqNbr A number that can be used to define a sequence in the strings
     *  	   to allow the receiver to check that no message was lost. It is
     *  	   transported as byte 2 in the payload.
     */
    virtual void openStringMessage(uint8_t type, uint8_t seqNbr=0xFF);

    /** Terminate and send a string message. This method must be used after a call
     *  to openStringMessage and the streaming of the string itself using operator<<.
     *  It adds the final \0 delimiter and sends the string.
     */
    virtual bool closeStringMessage(int timeOutCheckResponse = 300);

   /** Extract data from the payload to fill a string buffer.
   	* @param string The buffer to fill with the null-terminated string.
   	* 		 allocated by the caller, must be at least dataSize bytes.
   	* @param stringType The frame type found in the payload.
   	* @param seqNbr The optional sequence number associated with the string.
   	* @param data Payload pointer
   	* @param dataSize Payload size
   	* @pre The payload is expected to be exactly a byte with value
   	* 	   followed by an unused byte and a null-terminated string.
   	* @return True if operation was successful, false otherwise.
   	*/
      bool getString(char* string, uint8_t &stringType, uint8_t& seqNbr,
      		const uint8_t* data,uint8_t dataSize) const;
    ///@}

    /** @name Sleep mode control
     *  Those functions are used to control pin sleep (i.e. sleep mode controlled by
     *  the SLEEP_RQ pin of the XBee module.
     */
    ///@{

    /** Configure the microcontroller pins connected to the XBee SLEEP_RQ and ON/SLEEP pins.
     *  When set to a non-zero value of theSleepRequestPin:
     *  If useSleepMode==true :
     *    The XBee module is be configured for pin sleep, and will be
     *    woken up before every interaction
     *    (send & receive, printConfigurationSummary, checkXBeeModuleConfiguration, setTransmissionPower... )
     *    and put to sleep again after most of them (send, receive, setTransmissionPower etc.)
     *    if it was originally asleep.
     *    NB: the XBee is not put out of sleep by the individual AT_Commands methods.
     *    NB: the XBee is not put to sleep by this method.
     *  Else the XBeeModule is woken up, and remains active.
     *  When set with a non-zero value of the OnSleepPin (whatever the other parameters):
     *    The XBee module is configured to output the ON/SLEEP signal (pin #13).
     *  @Warning When the configuration results in a change of SM from 0 to 1 or conversely,
     *           this methods actively waits for the module to rejoin the network
     *           before returning. This can be a 5 to 6 seconds.
     *
     *  @param theSleepRequestPin The processor digital pin connected to the XBee SLEEP_RQ pin.
     *  						  Value 0 = pin not connected.
     *  @param theOnSleepPin The processor digital pin connected to the XBee ON_SLEEP pin (#13)
     *  					 Value 0 = pin not connected.
     *  @param useSleepMode If true, the
     */
    virtual void configureSleepMngtPins(uint8_t theSleepRequestPin, uint8_t theOnSleepPin, bool useSleepMode=true);    /** Take the XBee module out of sleep mode, and wait for 1 msec so it is
     *  fully operational.
     *  @return true if the module was in sleep mode and actually woken up, false
     *  		otherwise.
     */
    bool wakeUpXBeeModule();

    /** Put the XBee module to sleep. Actual sleep is entered after 12 msec, but
     *  the method only waits if argument "wait" is true.
     *  @param wait If true, wait until the module is actually sleeping before returning.
     *  @return true if the module was active and actually entered up sleep mode,
     *          false otherwise.
     */
    bool putXBeeModuleToSleep(bool wait=false);

    /** Read whether the XBee module is currently in sleep mode (pin sleep).
     *  @return true if in sleep mode.
     */
    bool ModuleInSleepMode() const;
    ///@}

    /** Send a parameterless AT command the XBee module.
     *  @param cmd  The command to run (it is expected to by a two-characters string).
     *  			This method does not  check whether the command makes sense.
     *  @param destSH	The serial number high of the Xbee module to send the command to.
     *   			(sh=sl=0 is the local
     *  			module).
     *  @param destSL	The serial number low of the Xbee module to send the command to
     *   			(sh=sl=0 is the local
     *  			module).
     *  @return true if command was successful, false otherwise.
     */
     bool sendAT_Command(const char* cmd,
    		 	 	 	 const uint32_t &destSH = 0,
    		             const uint32_t &destSL = 0) { uint8_t size=0; uint8_t value[1];
     									return queryParameter(cmd, value,size,destSH,destSL);};

   	/** Print the content of the payload in human-readable format on the Serial interface
   	 *  (for debugging only).
   	 *  @param data Payload pointer
   	 *  @param dataSize Payload size
   	 */
    static void printFrame(uint8_t* data, uint8_t dataSize);


    /** Send a Node Discovery (ND) command and print information about all
     *  discovered nodes.
     */
    void printDiscoveredNodes();

   	/** Print the content of the payload when containing a string
   	 *  (for debugging only).
   	 *  @param data Payload pointer
   	 *  @param dataSize Payload size
   	 */
    virtual void printString(uint8_t* data, uint8_t dataSize) const;

    /** Print a complete summary of the configuration parameters on the provided
     *  stream. The summary ends with a carriage return. The summary includes all
     *  information printed by other printXXXX methods.
     *  This method wakes up the XBee module, if it is in sleep mode, and puts
     *  it back to sleep afterwards, if it was previously sleeping.
     *  @param stream The stream to send the summary to.
	 *  @param sh	The serial number high of the Xbee module to query (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to query (sh=sl=0 is the local
	 *  			module).
     *  @return true if the operation was successful, false otherwise.
     */
    virtual bool printConfigurationSummary(Stream &stream,const uint32_t & sh=0, const uint32_t& sl=0);

    /** Print the network-related settings and diagnostic. The output ends with a carriage return.
     *  This method wakes up the XBee module, if it is in sleep mode, and puts
     *  it back to sleep afterwards, if it was previously sleeping.
     *  @param stream The stream to send the summary to.
 	 *  @param sh	The serial number high of the Xbee module to query (sh=sl=0 is the local
 	 *  			module).
 	 *  @param sl	The serial number low of the Xbee module to query (sh=sl=0 is the local
 	 *  			module).
     *  @return true if the operation was successful, false otherwise.
     */
     virtual bool printNetworkParameters(Stream &stream,const uint32_t & sh=0, const uint32_t& sl=0);

     /** Print the sleep mode settings. The output does end with a carriage return.
      *  This method wakes up the XBee module, if it is in sleep mode, and puts
      *  it back to sleep afterwards, if it was previously sleeping.
      *  @param stream The stream to send the summary to.
      *  @param sh	The serial number high of the Xbee module to query (sh=sl=0 is the local
      *  			module).
      *  @param sl	The serial number low of the Xbee module to query (sh=sl=0 is the local
      *  			module).
      *  @return true if the operation was successful, false otherwise.
      */
     virtual bool printSleepModeConfiguration(Stream &stream, const uint32_t & sh=0, const uint32_t& sl=0);

    /** Print the list of scanned channels in human-readable form, from the
     *  value of SC parameter. The output does not include a final end-of-line.
     *  @param stream   The stream to print to.
     *  @param SC_Value The value of the SC parameter.
     */
    static void printScannedChannels(Print &stream, uint16_t SC_Value);

    /** Print the sleep mode configuration in human-readable form, from the
     *  value of the SM parameter. The output does not include a final end-of-line.
     *  @param stream   The stream to print to.
     *  @param SM_Value The value of the SM parameter.
     */
    static void printSleepModeConfigurationLabel(Print &stream, uint8_t SM_Value);


    /** The maximum number of bytes that can be transported in a single call to send() */
    static constexpr uint8_t MaxPayloadSize=MAX_FRAME_DATA_SIZE-16;
    /** The maximum number of characters that can be transported in a single string */
    static constexpr uint8_t MaxStringSize=MaxPayloadSize-3;

    // ------------------------------------------------------------------
    /** @name Querying XBee parameters and module status
     *  Methods to obtain configuration information from an XBee module
     *  using (remote) AT commands through the serial interface. */
    // ------------------------------------------------------------------
    ///@{
    /** Detect whether or not a module is online (dh=dl=0 denotes the local
     *  module.
 	 *  @param sh	The serial number high of the Xbee module to query (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to query (sh=sl=0 is the local
	 *  			module).
     *  @return true if the module is online and responding, false otherwise.
     */
    bool isModuleOnline(const uint32_t& sh=0, const uint32_t& sl=0);

    /** Wait until the XBee module joined a network.
     *  This function is blocking and will never return if no network can be joined.
     */
    void waitForNetworkJoining();

    /** Obtain the device type of the XBee module.
	 *  @param sh	The serial number high of the Xbee module to query (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to query (sh=sl=0 is the local
	 *  			module).
     *  @return the current device type.
     */
    XBeeTypes::DeviceType getDeviceType(const uint32_t& sh=0, const uint32_t& sl=0);

    /** Obtain the current baud rate used on the serial interface used by the (local)
     *   XBee module, as well as the corresponding value for the BD setting.
     *   This method is used when matching the setting of the board-side Serial
     *   object is required.
     *  @param baudRate The current baud rate (bits/sec)
     *  @param xbeeSetting The corresponding baudrate code for the BD command.
     *  @return true if command was successful, false otherwise.
     */
     bool getXBeeSerialBaudRate(long &baudRate, uint8_t &xbeeSetting);


    /** Obtain the current value of a parameter. This method is synchronous and will use
	 *  the XBee module exclusively, discarding any incoming frame as long as the response
	 *  to the query is not received. In case of error, the operation will be retried at
	 *  most Retries times.
	 *  @param param The parameter to query (it is expected to by a two-characters string).
	 *  			 This method does not  check whether the parameter makes sense.
	 *  @param value An array of bytes allocated by the caller, to be filled by the XbeeClient
	 *  			 with the value.
	 *  @param valueSize The size of the value array. This value is updated by the XBeeClient
	 *                   to the number of bytes actually filled in the array (only if
	 *                   return value is true). If the value does not fit in the array,
	 *                   the method returns false and does not modify the array.
	 *  @param sh	The serial number high of the Xbee module to query (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to query (sh=sl=0 is the local
	 *  			module).
	 *  @param maxRetries The maximum number of retries, in case the operation fails for
	 *                    any reason.
	 *  @return true if query was successful, false otherwise.
	 *  @note If valueSize = 0, then a null-size response by the XBee module
	 *        is accepted in order for the method to support parameterless
	 *        commands. Otherwise, null-size responses are supposed to be an error.
	 */
    bool queryParameter(const char* param,
    					uint8_t value[], uint8_t &valueSize,
						const uint32_t& sh=0, const uint32_t& sl=0,
						uint8_t maxRetries=DefaultMaxAT_Retries);

    /** Same as QueryParameter(const char* param,uint8_t value[], uint8_t &valueSize)
     *  but the value is converted in an uint64_t value. This method should not be used
     *  for parameters with values of more than 8 bytes.
     *  @param param The parameter to query (it is expected to by a two-characters string).
     *  @param value The parameter's value.
     *  @return true if query was successful, false otherwise.
     */
    bool queryParameter(const char* param,uint64_t &value,const uint32_t& sh=0, const uint32_t& sl=0);
    /** See similar method for uint64_t value */
    bool queryParameter(const char* param, uint32_t &value,const uint32_t& sh=0, const uint32_t& sl=0);
    /** See similar method for uint64_t value */
    bool queryParameter(const char* param, uint16_t &value,const uint32_t& sh=0, const uint32_t& sl=0);
    /** See similar method for uint64_t value */
    bool queryParameter(const char* param, uint8_t &value,const uint32_t& sh=0, const uint32_t& sl=0);

    /** Same as QueryParameter(const char* param,uint8_t value[], uint8_t &valueSize)
	 *  but the value is converted in a boolean value. This method should not be used
	 *  for parameters with values of more than 1 bytes.
	 *  @param param The parameter to query (it is expected to by a two-characters string).
	 *  @param value The parameter's value.
	 *  @param sh	The serial number high of the Xbee module to query (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to query (sh=sl=0 is the local
	 *  			module).
	 *  @return true if query was successful, false otherwise.
	*/
    bool queryParameter(const char* param,bool &value,const uint32_t& sh=0, const uint32_t& sl=0);

    /** Same as QueryParameter(const char* param,uint8_t value[], uint8_t &valueSize)
	 *  but the value is converted in a string value. This method should not be used
	 *  for parameters with values of more than valueSize-1 bytes.
	 *  @param param The parameter to query (it is expected to by a two-characters string).
	 *  @param value The parameter's value. This array must be allocated by the caller.
	 *  @param valueSize The size of the value[] array.
	 *  @param sh	The serial number high of the Xbee module to query (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to query (sh=sl=0 is the local
	 *  			module).
	 *  @return true if query was successful, false otherwise.
	*/
    bool queryParameter(const char* param, char value[], uint8_t valueSize,
    		 const uint32_t& sh=0, const uint32_t& sl=0);
    ///@}


    // ------------------------------------------------------------------
    /** @name Configuring XBee parameters
     *  Methods to update the  configuration of the XBee module
     *  using AT commands through the serial interface. */
    // ------------------------------------------------------------------
    ///@{
   /** Configure the transmission power. If in sleep mode, the module is woken up,
    * and put to sleep again afterwards.
 	* @param level The requested power level.
 	* @param sh	The serial number high of the Xbee module to configure (sh=sl=0 is
 	*  			 the local module).
 	*  @param sl The serial number low of the Xbee module to configure (sh=sl=0 is
 	*  			 the local module).
 	* @return True if operation was successful, false otherwise.
 	*/
     bool setTransmissionPower(XBeeTypes::TransmissionPowerLevel level,
     		const uint32_t& sh=0, const uint32_t& sl=0);

    /** Configure the device type of the XBee module by setting parameters
     *  CE and SM.
     * @warning When setting the type to End-Device, the SM parameter is
     * 		 set to 1 (Pin sleep enabled). If another value of SM is needed,
     * 		 be sure to configure it AFTER setting the device type.
	 *  @param sh	The serial number high of the Xbee module to set (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to set (sh=sl=0 is the local
	 *  			module).
     * @param type The device type to set.
     * @return True if the operation was successful, false otherwise.
     */
    bool setDeviceType(XBeeTypes::DeviceType type,const uint32_t& sh=0, const uint32_t& sl=0);

    /** Set a parameter value. This method is synchronous and will use
     *  the XBee module exclusively, discarding any incoming frame as long as the response
     *  to the query is not received. In case of error, the operation will be retried at
	 *  most Retries times.
	 *  The change will not survive an XBee module reset. To make it permanent call the
	 *  SaveConfiguration() method.
     *  @param param The parameter to set (it is expected to by a two-characters string).
     *  			 This method does not  check whether the parameter makes sense.
     *  @param value An array of bytes allocated by the caller, which is the value to be set.
     *  @param valueSize The size of the value array containing relevant data for the set.
     *  @param sh	The serial number high of the Xbee module to set (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to set (sh=sl=0 is the local
	 *  			module).
     *  @param maxRetries The maximum number of retries, in case the operation fails for
	 *                    any reason.
     *  @return true if query was successful, false otherwise.
     */
    bool setParameter(const char* param, uint8_t value[], uint8_t valueSize,
    		const uint32_t& sh=0, const uint32_t& sl=0, uint8_t maxRetries=DefaultMaxAT_Retries);

    /** Same as SetParameter(const char* param, uint8_t value[], uint8_t valueSize) but
     *  value is provide as an uint64_t
     *  @param param The parameter to set (it is expected to by a two-characters string).
     *  			 This method does not  check whether the parameter makes sense.
     *  @param value The value to set to.
     *  @param sh	The serial number high of the Xbee module to set (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to set (sh=sl=0 is the local
	 *  			module).
     *  @return true if query was successful, false otherwise.
     */
    bool setParameter(const char* param, uint64_t value,
    		const uint32_t& sh=0, const uint32_t& sl=0);
    /** See similar method for uint64_t value */
    bool setParameter(const char* param, uint32_t value,
    		const uint32_t& sh=0, const uint32_t& sl=0);
    /** See similar method for uint64_t value */
    bool setParameter(const char* param, uint16_t value,
    		const uint32_t& sh=0, const uint32_t& sl=0);
    /** See similar method for uint64_t value */
    bool setParameter(const char* param, uint8_t value,
    		const uint32_t& sh=0, const uint32_t& sl=0);

    /** Same as SetParameter(const char* param, uint8_t value[], uint8_t valueSize) but
     *  value is provide as a string.
     *  @param param The parameter to set (it is expected to by a two-characters string).
     *  			 This method does not  check whether the parameter makes sense.
     *  @param value The value to set to.
     *  @param sh	The serial number high of the Xbee module to set (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to set (sh=sl=0 is the local
	 *  			module).
     *  @return true if query was successful, false otherwise.
     */
    bool setParameter(const char* param, const char* value,
    		const uint32_t& sh=0, const uint32_t& sl=0);

    /** Same as SetParameter(const char* param, uint8_t value[], uint8_t valueSize) but
     *  value is provide as a boolean
     *  @param param The parameter to set (it is expected to by a two-characters string).
     *  			 This method does not  check whether the parameter makes sense.
     *  @param value The value to set to.
     *  @param sh	The serial number high of the Xbee module to set (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to set (sh=sl=0 is the local
	 *  			module).
     *  @return true if query was successful, false otherwise.
     */
    bool setParameter(const char* param, bool value,
    		const uint32_t& sh=0, const uint32_t& sl=0);

    /** Permanently save the current configuration of the XBee module (WR command)
     *  Writes parameter values to non-volatile memory so that parameter modifications persist
     *  through subsequent resets.
     *  @return true if command was successful, false otherwise.
     */
    bool saveConfiguration(const uint32_t& sh=0, const uint32_t& sl=0) {
    	return sendAT_Command("WR", sh, sl);

    };
    ///@}

#undef NON_OPERATIONAL_METHOD
#ifdef NON_OPERATIONAL_METHOD
    /** Restore default configuration of the XBee module (RE command)
     *  It sets all parameters except ZS and KY to their default values.
     *  To change ZS and KY, you must explicitly set them. In order for the default
     *  parameters to persist through subsequent resets, call SaveConfiguration to
     *  send a separate WR command after RE.
     *  Read-only parameters are not directly affected by RE and reflect the current
     *  state of the device.
     *  This method restores the original serial baudrate after restoring parameters.
     *  @warning METHOD NOT OPERATIONAL.
     *  	- Test is included in test_XBClient_AT_Command (but inactive).
     *  	- For some reason, we cannot reestablish contact with the XBee
     *  	  module after a restore without a physical power down.
     *  	  Does the restore change the firmware? To be investigated.
     *  @return true if command was successful, false otherwise.
     */
    bool RestoreDefaultConfiguration();
#endif

    /** Perform a software reset of local XBee module (RE command)
     *  Resets the device. The device responds immediately with an OK and performs
     *  a reset 100 ms later. If you issue this command while the device is in
     *  Command Mode, the reset effectively exits Command mode.
     *  The command returns after 200 msec to guarantee the reset happened.
     *  @return true if command was successful, false otherwise.
     */
     bool softwareReset();

     // ------------------------------------------------------------------
     /** @name Checking/fixing XBee configuration
      *  Methods to check whether the value of parameter param is value,
      *  and possibly update it (non permanently) */
     // ------------------------------------------------------------------
     ///@{
 	/** Utility method to define the role allocated to the XBee module
 	 *  and check its configuration. This method should not be overridden by
 	 *  subclasses.
 	 *  It wakes up the XBee module, if it is in sleep mode, and puts it to sleep
 	 *  afterwards.
	 *  @param sh	The serial number high of the Xbee module to check (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to check (sh=sl=0 is the local
	 *  			module).
  	 *  @return true if the module was identified and the configuration is ok.
 	 *  		false otherwise.
 	 */
 	 bool checkXBeeModuleConfiguration(const uint32_t& sh=0, const uint32_t& sl=0 ) ;

	/** Utility method to define the role allocated to the XBee module,
 	 *  check the configuration and possibly update it (permanently).
 	 *  This method should not be overridden by subclasses.
 	 *  @param correctConfiguration If true, and the XBee is referenced in GMiniConfig.h,
 	 *         						update configuration if it is not correct.
 	 *  @param configurationChanged (out) This argument is set to true if anything
 	 *	 								  was actually updated.
 	 *  @param sh	The serial number high of the Xbee module to check (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to check (sh=sl=0 is the local
	 *  			module).
 	 *  @return true if the module was identified and the configuration is ok.
 	 *  		true if the module was identified and the configuration was not ok,
 	 *  		     but successfully updated
 	 *  		false otherwise.
 	 */
 	 bool checkAndFixXBeeModuleConfiguration(bool &configurationChanged, const uint32_t& sh=0, const uint32_t& sl=0) {
 		return doCheckXBeeModuleConfiguration(true, configurationChanged, sh, sl);
 	};

     /** Check whether the value of parameter param is value, possibly update it
      *  (non permanently)
      *	 @param param The parameter to check
      *	 @param value The expected value for the parameter
      *	 @param correctConfiguration If true, an incorrect configuration value
      *	 							 will be fixed.
      *	 @param configurationChanged (out) This argument is set to true if the
      *	 							 parameter was actually updated.
      *  @param sh	The serial number high of the Xbee module to check (sh=sl=0 is the local
      *  			module).
      *  @param sl	The serial number low of the Xbee module to check (sh=sl=0 is the local
      *  			module).
      *	 @return True if either the parameter was correct, or was succesfully fixed.
      *	 		 False otherwise.
      */
     bool checkParameter(	const char* param,
    		 	 	 	  	uint64_t value,
							bool correctConfiguration,
							bool &configurationChanged,
							const uint32_t& sh=0, const uint32_t& sl=0 );

     /** See similar method with uint64_t */
     bool checkParameter(	const char* param,
    		 	 	 	  	uint32_t value,
							bool correctConfiguration,
							bool &configurationChanged,
							const uint32_t& sh=0, const uint32_t& sl=0 );

     /** See similar method with uint64_t */
     bool checkParameter(	const char* param,
    		 	 	 	  	uint16_t value,
							bool correctConfiguration,
							bool &configurationChanged,
							const uint32_t& sh=0, const uint32_t& sl=0 );
     /** See similar method with uint64_t */
     bool checkParameter(	const char* param,
    		 	 	 	  	uint8_t value,
							bool correctConfiguration,
							bool &configurationChanged,
							const uint32_t& sh=0, const uint32_t& sl=0 );
     /** See similar method with uint64_t */
     bool checkParameter(	const char* param,
							const char* value,
							bool updateConfiguration,
							bool &configurationChanged,
							const uint32_t& sh=0, const uint32_t& sl=0 );
     /** See similar method with uint64_t */
     bool checkParameter(	const char* param,
    		 	 	 	  	bool value,
							bool correctConfiguration,
							bool &configurationChanged,
							const uint32_t& sh=0, const uint32_t& sl=0 );

     /** Check whether the device type is the provided value, possibly update it
      *  (non permanently)
      *	 @param value The expected device type
      *	 @param correctConfiguration If true, an incorrect configuration value
      *	 							 will be fixed.
      *	 @param configurationChanged (out) This argument is set to true if the
      *	 							 parameter was actually updated.
      *  @param sh	The serial number high of the Xbee module to check (sh=sl=0 is the local
      *  			module).
      *  @param sl	The serial number low of the Xbee module to check (sh=sl=0 is the local
      *  			module).
      *	 @return True if either the device type was correct, or was succesfully fixed.
      *	 		 False otherwise.
      */
     bool checkDevicetype( XBeeTypes::DeviceType value,
    		 	 	 	 	bool correctConfiguration,
							bool &configurationChanged,
							const uint32_t& sh=0, const uint32_t& sl=0 );
     ///@}

     /** Obtain a human-readable label for a particular device type.
      *  @param type The device type to label.
      *  @return The corresponding label. */
     static const char* getLabel(XBeeTypes::DeviceType type);


  protected:
    static constexpr bool DisplayOutgoingFramesOnSerial = false; /**< Activate for debugging only */
    static constexpr bool DisplayIncomingFramesOnSerial = false; /**< Activate for debugging only */
    static constexpr uint8_t DefaultMaxAT_Retries=2; /**< The maximum number of retries for AT commands */

    // ------------------------------------------------------------------
    /** @name Querying Internal XBee configuration. */
    // ------------------------------------------------------------------
    ///@{
    /** Helper method to query an integer value of numBytes bytes
      *  @param param The parameter to query (it is expected to by a two-characters string).
      *  			 This method does not  check whether the parameter makes sense.
      *  @param value The value to read (only the first numBytes are read.
      *  @param numBytes The number of bytes used in the value.
 	 *  @param sh	The serial number high of the Xbee module to query (sh=sl=0 is the local
 	 *  			module).
 	 *  @param sl	The serial number low of the Xbee module to query (sh=sl=0 is the local
 	 *  			module).
      *  @return true if query was successful, false otherwise.
      */
     bool queryIntegerParameter(const char* param, uint64_t &value, uint8_t numBytes,
     		const uint32_t& sh=0, const uint32_t& sl=0);

    /** Obtain the current value of a parameter. This method is synchronous and will use
	 *  the XBee module exclusively, discarding any incoming frame as long as the response
	 *  to the query is not received.
	 *  @param param The parameter to query (it is expected to by a two-characters string).
	 *  			 This method does not  check whether the parameter makes sense.
	 *  @param value An array of bytes allocated by the caller, to be filled by the XbeeClient
	 *  			 with the value.
	 *  @param valueSize The size of the value array. This value is updated by the XBeeClient
	 *                   to the number of bytes actually filled in the array (only if
	 *                   return value is true). If the value does not fit in the array,
	 *                   the method returns false and does not modify the array.
	 *  @param doNotRetry This is set to true when the operation fails and all retries will
	 *                    will necessarily fail (i.e. because the queried parameter does not exist).
	 *  @param sh	The serial number high of the Xbee module to query (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to query (sh=sl=0 is the local
	 *  			module).
	 *  @return true if query was successful, false otherwise.
	 *  @note If valueSize = 0, then a null-size response by the XBee module
	 *        is accepted in order for the method to support parameterless
	 *        commands. Otherwise, null-size responses are supposed to be an error.
	 */
    bool queryParameterOnce(const char* param,uint8_t value[], uint8_t &valueSize,
    						bool &doNotRetry, const uint32_t& sh=0, const uint32_t& sl=0);

    bool queryRemoteParameterOnce(const char* param, uint8_t value[], uint8_t &valueSize,
    						bool &doNotRetry, XBeeAddress64& remote);
    bool queryLocalParameterOnce(const char* param,uint8_t value[], uint8_t &valueSize,
    						bool &doNotRetry);
    ///@}

    // ------------------------------------------------------------------
    /** @name Setting Internal XBee configuration. */
    // ------------------------------------------------------------------
    ///@{
    /** Helper method to set an integer value of numBytes bytes
      *  @param param The parameter to set (it is expected to by a two-characters string).
      *  			 This method does not  check whether the parameter makes sense.
      *  @param value The value to set (only the first numBytes are set.
      *  @param numBytes The number of bytes used in the value.
      *  @return true if query was successful, false otherwise.
      */
     bool setIntegerParameter(const char* param, uint64_t value, uint8_t numBytes,
     		const uint32_t& sh=0, const uint32_t& sl=0);

    /** Set a parameter value. This method is synchronous and will use
     *  the XBee module exclusively, discarding any incoming frame as long as the response
     *  to the query is not received. The change will not survive an XBee module reset.
     *  To make it permanent call the SaveConfiguration() method.
     *  @param param The parameter to set (it is expected to by a two-characters string).
     *  			 This method does not  check whether the parameter makes sense.
     *  @param value An array of bytes allocated by the caller, which is the value to be set.
     *  @param valueSize The size of the value array containing relevant data for the set.
     *  @param doNotRetry This is set to true when the operation fails and all retries will
	 *                    will necessarily fail (i.e. because the parameter to be set does
	 *                    not exist).
     *  @param sh	The serial number high of the Xbee module to set (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to set (sh=sl=0 is the local
	 *  			module).
     *  @return true if query was successful, false otherwise.
     */
    bool setParameterOnce(const char* param, uint8_t value[], uint8_t valueSize,
    		bool &doNotRetry,
    		const uint32_t& sh=0, const uint32_t& sl=0);

    bool setLocalParameterOnce(const char* param,
    		uint8_t value[], uint8_t valueSize,
    		bool &doNotRetry);
    bool setRemoteParameterOnce(const char* param,
    		uint8_t value[], uint8_t valueSize,
    		bool &doNotRetry, XBeeAddress64 &remote);
    ///@}

    /** Display the response to a ND (Node discovery) AT command sent in
     *  API mode (no carriage returns).
     *  @param response Pointer to the response provided by the AT Command
     *  response.
     */
    static void printND_Response(const uint8_t* response);

	/** Utility method to define the role allocated to the XBee module,
 	 *  check the configuration and possibly update it (permanently).
 	 *  This method is empty and should be implemented by subclasses, and make use of the
 	 *  various checkXXX() methods (See example in CansatXBeeClient, for instance).
 	 *  @param correctConfiguration If true, and the XBee is referenced in GMiniConfig.h,
 	 *         						update configuration if it is not correct.
 	 *  @param configurationChanged (out) This argument is set to true if anything
 	 *	 								  was actually updated.
	 *  @param sh	The serial number high of the Xbee module to check (sh=sl=0 is the local
	 *  			module).
	 *  @param sl	The serial number low of the Xbee module to check (sh=sl=0 is the local
	 *  			module).
 	 *  @return true if the module was identified and the configuration is ok.
 	 *  		true if the module was identified and the configuration was not ok,
 	 *  		     correctConfiguration=true and configuration was successfully updated
 	 *  		false otherwise.
 	 */
 	virtual bool doCheckXBeeModuleConfiguration(bool correctConfiguration,
 									  bool &configurationChanged,
									  const uint32_t& sh=0, const uint32_t& sl=0 );
 	 /* Print the value of an integer parameter to the stream, with no final CR.
 	  * @param stream The stream to print to
 	  * @param name The name of the parameter (2 characters).
 	  * @param sh	The serial number high of the Xbee module to query (sh=sl=0 is the local
	  *  			module).
	  * @param sl	The serial number low of the Xbee module to query (sh=sl=0 is the local
	  *  			module).
 	  *
 	  * @return true if paremeter was successfully queried and printed.
 	  */
 	bool printIntegerParameter(Stream &stream, const char* name, const uint32_t & sh, const uint32_t& sl);

 	/** Automatically wake up and XBee module, if sleeping and remember it so
 	 *  autoPutXBeeModuleToSleep() will put it to sleep again. If the module was
 	 *  already active, autoPutXBeeModuleToSleep() will be noop.
 	 *  Always call in pair with autoPutXBeeModuleToSleep().
 	 *  @return true if the module was in sleep mode and actually woken up, false
     *  		otherwise.
 	 */
 	bool autoWakeUpXBeeModule();

 	 /** Automatically put the XBee module to sleep only if the last call to
 	  *  autoWakeUpXBeeModule() actually did wake up a sleeping module.
 	  *  Always call in pair with autoWakeUpXBeeModule().
      *  @return true if the module was in sleep mode and actually woken up, false
      *  		 otherwise.
 	  */
 	bool autoPutXBeeModuleToSleep(bool wait=false);
  private:
 	static constexpr uint8_t RouteRecordIndicator=0xA1; // apiID.
 	static constexpr uint8_t ExtendedTransmitStatus=0x8B; // apiID.

    XBee xbee;
    XBeeAddress64 defaultReceiverAddress;
    String msgBuffer; /**<The internal buffer to build the string messages */
    StringStream sstr; /**< A stream to fill the buffer */
#ifdef ARDUINO_AVR_UNO
    SoftwareSerial* swSerialStream; /**< The object managing the serial port to the XBee module, when
     	 	 	 	 	 	 	 	   using a software serial port  */
#endif
    HardwareSerial* hwSerialStream; /**< The object managing the serial port to the XBee module, when
     	 	 	 	 	 	 	 	   using an hardware UART*/
    uint8_t sleepRequestPin;		/**< The ID of the µC pin connected to the SLEEP_RQ pin of the
    									 XBee module. 0=Unconnected. */
    uint8_t onSleepPin;				/**< The ID of the µC pin connected to the ON/SLEEP pin of the
     									 XBee module. 0=Unconnected. */
    uint8_t transparentWakeUpPerformed=0;
    								/**< How many times was autoWakeUpXBeeModule() called, starting
    								     with a really useful wake up? This is used to define when
    								     autoPutXBeeModuleToSleep should actually trigger sleep mode.
    									 It is increased for each call to autoWakeUp, from the moment
    									 sleep mode is actually interrupted, and decreased by each call
    									 to autoPutXBeeModuleToSleep(), triggering sleep mode when reaching
    									 zero */
    template<class T> friend XBeeClient &operator <<(XBeeClient &xb, T arg);
    friend class TestXBeeClient;
};

template<class T> inline XBeeClient &operator <<(XBeeClient &xb, T arg) {
  xb.sstr << arg; return xb;
}

#endif
