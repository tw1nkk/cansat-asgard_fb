#pragma once
#include "AsyncServoWinch.h"

/** @ingroup CSPU_Servos
    @brief Subclass of AsyncServoWinch that is specialised for the SW5513-6MA servo winch.
*/
class AsyncServoWinchSW55136MA : public AsyncServoWinch {
  public:
    AsyncServoWinchSW55136MA();

    uint16_t getRopeLenFromPulseWidth(uint16_t pulseWidth) const override;

    uint16_t getPulseWidthFromRopeLen(uint16_t ropeLen) const override;


  private:
    /** Get the size of the SW55136MA_RopeLenToPulseWidth conversion table
      @return the size of the conversion table
    */
    static uint8_t getConversionTableSize();

    static constexpr uint16_t SW55136MA_MaxRopeLen = 500;		/**< The rope length when fully extended for the SW5513-6MA servo winch. (mm) */
    static constexpr uint16_t SW55136MA_MinPulseWidth = 1000;	/**< The minimum pulse width for the SW5513-6MA servo winch.*/
    static constexpr uint16_t SW55136MA_MaxPulseWidth = 2000;	/**< The maximum pulse width for the SW5513-6MA servo winch.*/

    static constexpr uint16_t SW55136MA_RopeLenToPulseWidth[] = {1000, 1018, 1035 /*(approximation)*/, 1053 /*(approximation)*/, 1071, 1102, 1127, 1147, 1166, 1191, 1220, 1244, 1276, 1303, 1332, 1359, 1383, 1407, 1436, 1457, 1472, 1491}; /**< The conversion table rope length -> pulse width. SW55136MA_RopeLenToPulseWidth[i] is the pulse width when the rope is extended by i cm */

    friend class ServoWinch_Test;	/**< Friend class used for testing */
};
