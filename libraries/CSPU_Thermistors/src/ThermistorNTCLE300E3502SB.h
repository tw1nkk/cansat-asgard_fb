/*
   ThermistorNTCLE300E3502SB.h
*/

#pragma  once
#include "ThermistorSteinhartHart.h"

/** @ingroup CSPU_Thermistors
  @brief This a subclass of ThermistorSteinhartHart customized for thermistor NTCLE305E4502SB.
  Use this class to read thermistor resistance and convert to degrees.
  Wiring: VCC to thermistor, thermistor to serialresistor, serialresistor to ground.
*/

class ThermistorNTCLE300E3502SB: public ThermistorSteinhartHart {
  public:
    /**
      @param theVcc  The voltage supplied to the serial resistor+thermistor assembly
      @param theAnalogPinNbr The pin connected to the thermistor-to-resistor connexion.
      @param theSerialResistor The value of the resistor connected to the thermistor (ohms).
    */
    ThermistorNTCLE300E3502SB(float theVcc, byte theAnalogPinNbr, float theSerialResistor ):
      ThermistorSteinhartHart(theVcc, theAnalogPinNbr, 5000.0, 0.003354016,  0.0002565236, 2.605970E-06, 6.329261E-08, theSerialResistor) {};
  virtual ~ThermistorNTCLE300E3502SB(){};

};
