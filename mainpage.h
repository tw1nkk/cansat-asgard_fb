/**  \mainpage On-board software documentation
 *
 *
 * This is the documentation of the software written by successive Cansat projets in Collège Saint-Pierre, Uccle.
 * 
 * It is extracted from the source code by the Doxygen® tool. 
 *
 * \section srcOrg Organisation of source code
 *
 * Our source code abides by a strict organisation of folders and files which is described in
 * document <em>Arduino Development Organisation</em> located in folder 2400 (Can Software) of our 
 * project folder. Be sure to read if before contributing. 
 *
 * \section step1 Using the on-board software
 *
 * Before contributing or using the on-board software, a number of products must be installed and
 * configured on your computer (ArduinoIDE, SourceTree, Doxygen...). Please refer to document 
 * <em>Getting Started with Cansat</em> in the root folder of Cansat0000_CSPU.
 */