/*
 * xxxCanCommander.h
 */

#pragma once
#include "RT_CanCommander.h"
#include "CansatInterface.h"

/** @brief A subclass of RT_CanCommander for an hypothetical xxx project.
 *  It should add support for the xxx-specific commands
 *  as specified in CansatInterface.h 
 *  This dummy class demonstrate the architecture of a Cansat project
 *  in conjunction with xxxMainWithRT-CanCommander sketch. 
 *
 *  See examples in TorusCanCommander and next projects.
 */
class xxxCanCommander : public RT_CanCommander {
public:
	xxxCanCommander(unsigned long int theTimeOut): RT_CanCommander(theTimeOut) {};
	virtual ~xxxCanCommander() {};

	// Possible override begin() for xxx-specific additional initialization
	// See detailed description in base class.
#ifdef RF_ACTIVATE_API_MODE
     void begin(CansatXBeeClient& xbeeClient, SdFat* theSd = NULL, CansatAcquisitionProcess* theProcess = NULL
    		 /*  possible additional parameters */ ) {
    	 RT_CanCommander::begin(xbeeClient,theSd,theProcess);
    	 // Additional initialization here if any.
     } ;
#else
     void begin(Stream& theResponseStream, SdFat* theSd = NULL, CansatAcquisitionProcess* theProcess = NULL
    		 /* possible additional parameters */ ) {
    	 RT_CanCommander::begin(theResponseStream,theSd,theProcess);
    	 // Additional initialization here if any.
     };
#endif


protected:
     // Override this method to add processing of xxx-specific  commands
     virtual bool processProjectCommand(CansatCmdRequestType requestType, char* cmd) override {
    	 bool result = true;

    	 switch (requestType) {
    	 /*
    	 case CansatCmdRequestType::xxxx:
    		 processReq_CmdXXXXX(cmd);
    		 break;
    	 case CansatCmdRequestType::yyyyy:
    		 processReq_CmdYYYYY(cmd);
    		 break;
    	 */
    	 default:
    		 // Do not report error here: this is done by the superclass.
    		 result = false;
    	 } // Switch
    	 return result;

     };
     /** Perform any action required before the can is powered down
      * @return True if everything went as planned.
      */
     virtual bool prepareSecondaryMissionForShutdown() override {return true;} ;

     /** Cancel any action performed to prepare the can for powering down,
      *  so normal operation can be resumed.
      * @return True if everything went as planned.
      */
     virtual bool cancelSecondaryMissionShutdown() override { return true;} ;

     void processReq_CmdXXXXX(char* &nextCharAddress) {};
     void processReq_CmdYYYYY(char* &nextCharAddress) {};
private:
     // xxx-specific data if any.
};
