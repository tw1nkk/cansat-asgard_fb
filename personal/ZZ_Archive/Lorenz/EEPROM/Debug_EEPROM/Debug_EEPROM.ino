/*
  ------------------------------------------------------------------------------------------
  --------------------------Debug EEPROM----------------------------------------------------

    Testing program for validating the behaviour of EEPROM_Bank and EEPROM_BankWriter
    when writing numerous records in a large memory.
    This program is written to track the issue resulting in the memory corruption observed
    during the ISATIS flight, which suggests that in some circumstances, the header or
    the reading/writing pointers are not correctly maintained.

    Begun on the 6th August 2018 by Lorenz Veithen
  ------------------------------------------------------------------------------------------
  ------------------------------------------------------------------------------------------
*/
#include "Arduino.h"
#include "EEPROM_BankWithTools.h"
#include "EEPROM_Bank.h"
#include "EEPROM_BankWriter.h"
#include "HardwareScanner.h"
#define DEBUG_CSPU
#include "DebugCSPU.h"

#define USE_ASSERTIONS
//#define SIZES

// The record that will be written and read again from memory
byte myRecord[255];

// Function for init of myRecord
void initOfMyRecord() {
  for (int e = 0; e <= 255; e++) {
    myRecord[e] = e + 1;
  }
}


void checkOneRecordSize(unsigned long recordSize) {

  // Counter of problems
  int nbProblems = 0;

  // EEPROM_KeyValue
  unsigned int EEPROM_KeyValue = recordSize;

  // Objects
  EEPROM_BankWithTools eb(5);
  HardwareScanner hw;

  // Init
  hw.init(10, 100);
  Serial <<  "Initialisation of EEPROM_Bank object...";
  bool resultInit = eb.init(EEPROM_KeyValue, hw, recordSize);

  // Check up init
  if (!resultInit) {
    Serial << "NOPE !" << ENDL;
    nbProblems++;
    Serial.flush();
    exit(0);
  }
  else {
    Serial << "OK !" << ENDL;
  }

  // Clear EEPROMs
  Serial << "Clearing EEPROMS...";
  eb.erase();
  Serial << "Cleared !" << ENDL;

  // Màj header
  Serial << "Updating header...";
  eb.doIdle(true);
  Serial << "OK !" << ENDL;

  // Init a counter
  unsigned long count1 = 0;

  // Fill memory with as many records as possible.
  Serial << "/-/-/ BEGINS STORING DATA /-/-/" << ENDL;
  Serial << "Storing Data..." << ENDL;

  // Using the counter
  while (eb.storeOneRecord(myRecord, recordSize)) {
    count1++;
    int y;
    y++;

    if (y == 1000) {
      Serial << "Still going on" << "(" << count1 << ")" << ENDL;
      y = 0;
    }
  }
  Serial << "Amount of records: " << count1 << ENDL;
  Serial << "Whole data stored !" << ENDL;

  // DoIdle
  Serial << "" << ENDL;
  Serial << "doIdle...";
  eb.doIdle(true);
  Serial << "OK !" << ENDL;
  Serial << "" << ENDL;

  // Init of myRecord
  initOfMyRecord();

  Serial << "(1) Checking number of records equals the theoretical one...";

  // Calculations of the theoretical number
  unsigned long totalSize = eb.getTotalSize(); // Total size in the memory system
  Serial << "Total size of the memory: " << totalSize << ENDL;
  unsigned long theoreticalNB = (totalSize - sizeof (eb.getHeader())) / recordSize; // Theoretical number of records

  // Check 1
  if (count1 == theoreticalNB) {
    Serial << "OK ! Right Number !" << ENDL;
    Serial << "" << ENDL;
  }
  else {
    Serial << "NOPE ! Unmatching numbers !" << ENDL;
    Serial << count1 << " != " << theoreticalNB << ENDL;
    Serial << "" << ENDL;
    nbProblems++;
    /*
      ==> Works perfectly
    */
  }

  // Reset of the reader
  Serial << "Reset reader...";
  eb.resetReader();
  Serial << "OK !" << ENDL;
  Serial << "" << ENDL;

  // Init of myRecord
  initOfMyRecord();

  Serial << "(2) Checking number of records written equals number of records read..." << ENDL;

  // recordsLeftToRead
  unsigned long RecordsLeftToRead = eb.recordsLeftToRead();
  Serial << "Records which are left to read: " << RecordsLeftToRead << ENDL;

  // Check 2
  if (count1 == RecordsLeftToRead) {
    Serial << "OK !" << ENDL; // Same number written as read
    Serial << "" << ENDL;
  }
  else {
    Serial <<  "NOPE !" << ENDL; // Not the same number written as read
    Serial << "" << ENDL;
    nbProblems++;
    /*
      ==> Works perfectly !
    */
  }

  // Printing of header
  Serial << "Printing header from chip...";
  eb.printHeaderFromChip();
  Serial << "OK !" << ENDL;
  Serial << "" << ENDL;

  // Init of myRecord
  initOfMyRecord();

  Serial << "(3) Checking size and data...";

  // Init counter
  unsigned long  count2 = 0;
  unsigned long count3 = 0;

  // Works until there no data available anymore
  while (!eb.readerAtEnd()) {

    // To be sure not to test on previous values
    for (int i = 0; i < recordSize; i++) {
      myRecord[i] = 0;
    }

    // Reading
    bool numBytes = eb.readOneRecord((byte*)& myRecord, recordSize);
    count2++;

    // Checks if the data match
    for (int t = 0; t < recordSize; t++) {
      count3++;
      if (myRecord[t] != t + 1) {
        if (count3 % recordSize == 0) {
          Serial << "NOPE ! PROBLEM: UNMATCHING DATA !" << "(" << count2 << ")" << ENDL;
          Serial << "" << ENDL;
          nbProblems++;
          /* THE PROBLEM IS HERE:
             Stops at address 255 (last "OK !")
             And the record 503

             ==> The bug should be in the readOneRecord() function or an underlying one
             The readerAtEnd() function never return true because the data in the second chip is never read...

             When the bug appears, it is said that leftToRead = 65535 which is the maximum value of an int...
             IT'S SUSPECT !!!

             Plus it appears to happen when you change of chip (see readData() )

             ==> Works perfectly for any recordSize
          */
        }
      }
      else {
        if (count2 % 1000 == 0) {
          if (count3 % recordSize == 0) {
            Serial << "OK !" << "(" << count2 << ")" << ENDL;
            Serial << "" << ENDL;
          }
        }
      }
    }

    // Checks if the sizes match
#ifdef SIZES
    if (numBytes == true) {
      if (count2 % 1000 == 0) {
        Serial << "OK for the sizes !" << ENDL;
        Serial << "" << ENDL;
      }
    }
    else {
      Serial << "NOPE ! Unmatching sizes" << ENDL;
      Serial << "" << ENDL;
      nbProblems++;
    }
#endif
  }

  if (nbProblems == 0) {
    Serial << F("Success for recordSize = ") << recordSize << ENDL;
    Serial << F("-- - Next test -- -") << ENDL;
  }
  else {
    Serial << F("Number of problems to solve: ") << nbProblems << "(for recordSize = " << recordSize << ")" << ENDL;
    Serial << F("--- Next test ---") << ENDL;
    Serial << "" << ENDL;
    Serial << "" << ENDL;
  }
}


void setup() {
  // Init Serial
  DINIT(19200);

  Serial << "------------------------------------------------------------ -" << ENDL;
  Serial << "Let's go" << ENDL;

  // Init myRecord
  Serial << "Init myRecord...";
  initOfMyRecord();
  Serial << "OK !" << ENDL;

  // The thing
  for (int i = 1; i <= 5; i++) {
    Serial << "//////////////////////////////////////////////////////////////" << ENDL;
    Serial << "--- Starting test for recordSize = " << i << "... ---" << ENDL;
    checkOneRecordSize(i);
  }
}

void loop() {
  delay(500);
}
