CansatRecord: OK
CansatConfig.h: OK
BMP280_Client: OK, testé
ThermistorClient Relu
   Renommé les classes qui implémentent SteinhartHart. 
   test_ThermistorSteinhartHart: relu, compile, A FAIRE TOURNER. 
   Créé un ThermistorClient qui gère 1 à 3 thermistors. Test écrit pour les 3 thermistors, SW arch à jour. 
GPS_Client: 
	Créé la classe GPS_Client dans cansatAsgard.
	Créé le programme de test. 
	Compilation OK. Test OK.
	Pour le timer: oui, il est crée même quand GPS_ECHO est false, et c'est NORMAL et indispensable. Documenté dans SW arch.
HardwareScanner: Rien à modifier.
XBeeClient
   Repris l'IsaTwoXBeeClient comme CansatXBeeClient. 
      les constantes protégées devraient être dans la superclasse FAIT et dans la sous-classe (PAS SÛR?), on ne doit spécialiser
      que pour le display des records dans send et receive.
      créé le display frame dans XBeeClient_Utilities (à nettoyer).  Utiliser un displayCansatPayload dans la sous-classe. 
      Le display doit couvrir tous les frame types.
      Fait, compilation OK.
      Test_XBeeClient-Send/Receive : Compile, test ok.
      Test_CansatXBeeClient-Receive: Compile, test ok.
      Test_CansatXBee-halfduplex: Compile, test ok.       	
RT_CanCommander :
	- Repris le RT_Commander comme RT_CanCommander.
	- API mode OK
	- Transparent mode OK 
	- Compilation sur Arduino à valider. OK.
	* MYSTERE A PERCER: RT_CanCommander 498: RF_Stream->XBeeClient::send((uint8_t*) buffer, (uint8_t) (read+3), 0);
 	  POURQUOI le XBeeClient:: est-il nécesaire ????
HardwareScanner: OK
	- Repris tout qui était utile dans IsaTwoHW_Scanner
	- Splitté en 2 classes
	- Dédoublé le sketch de utils.
	- Mis à niveau le test unitaire 
	- Testé sur Uno et Feather.  
	- Documenté dans l'architecture SW.
StorageMgr: OK
	- Utiliser le CansatRecord  FAIT
	- Désactiver les EEPROM si IGNORE_EEPROMS est défini. FAIT
	- Test OK. 
AcquisitionProcess
  - Créé LED_Type.h
  - Scope de AcquisitionProcess et CansatAcqProcess clarifié. OK
  - Test: OK. 
  - Intégré tout ce qu'il y a dans IsaTwo (générique)en un CansatAcquisitionProcess
  - Gérer dans l'acq. process un HardwareScanner et dans le CSTprocess un CansatHW_Scanner. FAIT.
  - remplacer les méthodes purement virtuelles par des méthodes vides:  FAIT.
  - Remplacer AcqOn AcqOff par une enum class dans LED_Type.h FAIT
  - Test de CansatAcquisitionProcess avec le main. 
MainWithRT_Commander:
  - OK.
SD_Logger: 
    - Utiliser le CansatRecord FAIT
    - OK.
RF_Transceiver:
    - Fait une version générique dans Templates.
	- Problème avec les LEDs uplink et hearbeat: arrangé (soudure cassée).
	- Toutes commandes testées. 
Review documentation cansatAsgard: OK
Docs à mettre à jour: 
  * System architecture (1200). Fait.
RF_Transceiver: prévoir deux modes: un ou l'output RT-Commander est brut, un où il ne l'est pas. FAIT!
Créer le TorusCanCommander pour les commandes de parachute.

Créer une classe CansatRecordTestFlow, qui fournit des records depuis un fichier csv.
   - données IsaTwo elsenborn disponibles (test_CansatRecordTestFlow/TestData.
      Test ok. Fusionné. 
Compléter le TorusRecord les données de controleur + créé le TorusInterface.h: OK, fusionné.
      
Complété le CansatRecord avec l'altitude de référence. TEST A TERMINER cf. plus bas. 
Support BMP388: OK
Configuration de l'analogReference: OK? 

========= IN PROGRESS ===========
	
Probleme des records séparés de plus de 70 msec.   
* performMaintenance n'est pas implémenté dans SD_Logger. C'est normal:
  pour le moment on ouvre et ferme le fichier à chaque écriture, ce qui 
  fait un flush/sync chaque fois. 
* Analyse du timing: l'écriture SD ne prend jamais plus de 16 msec, même quand
  le cycle dépasse les 70... Le loop() ne prend jamais plus de 31 msec ????? 
* Conclusion: le temps est passé dans des interruptions ou des tâches système non
  contrôlées par nous. Pour améliorer l'écriture SD (flux rapide et ininterrompu)
  il faut utiliser un système complexe de multiples buffers et contrôler exactement
  ce qui se passe. A voir pour l'été ? 
Reprendre le calcul de l'altitude de référence du TorusSecondaryMissionController, et
l'intégrer dans le BMP_Client. 
	* Branche créée.
	* Supprimé de SecondaryMissionController, intégré dans BMP.
	* Settings transférés de TorusConfig.h à CansatConfig.h
	* Compilation TorusMain, testBMP et testSecondaryMissionController OK. 
	* New test program prepared and tested.
	* Compilation ok with and without INCLUDE_REFERENCE_ALTITUDE defined in CansatConfig.h 

Test Arduino IDE v2.0beta10
    * Fonctionne, mais les cartes installées n'apparaissent pas dans le menu?  Probablement 
      un bug de la bêta. 
CansatMain: reprendre les améliorations pertinentes faites dans TorusMain. OK
			Faire du main un template ? Non
RF_Transceiver: reprendre les améliorations pertinentes faites dans TorusRF-Transceiver. OK
			En faire un template ? Non
			

Arduino-cli:
    * Je compile un programme, dans le répertoire,  avec 
      arduino-cli compile --fqbn adafruit:samd:adafruit_feather_m0_express --warnings all
      (alias: clic). On peut ajouter --clean pour une compilation à blanc et/ou -v (--verbose)
      
========= IN PROGRESS ==============
 
 
 A tester avec ItsyBitsy M4:  
 		Serial2 OK.
 		Timers: 
 		  Compilation de TC_Timers flanche: a été désactivé pour SAMD51. 
		   M4 (SAMD51) ne fonctionne pas de la même manière que M0 (SAMD21) pour les timers
		   Librairie SAMD_InterruptTimers installée et testée avec M0 et M4 (exemple ISR_TimerArray): OK
		   Créer un test explicite pour bien comprendre (test_ISR_TimersM0-M4) FAIT test_SAMD_InterruptTimers.
		   Une fois qu'on a un usage portable,  OK
		   		* améliorer le programme de test OK
		   		* déplacer vers CansatAsgard. OK
 		HW_Scanner OK.
 		CansatHW_Scanner, OK.
 		GPS  	* Tester le nouveau  GPS_Client (PREPARE, #define NEW_VERSION)
		   		* si OK sortir TC_Timer de la librairie. TODO.
 		ADC   Test in utils. M4 chip has a silicon bug. AREF tied to 3.3V, cannot be used. 
 			  Documented. Question asked to support. 
 		I2C?  A tester pour être sûr, avec un main torus, sur M4 pour s'assurer que
 			  Serial2 et I2C n'interfèrent pas de manière imprévue. 
 		Servo: KO,  La lib servo 1.1.6 utilisée par Torus ne supporte pas SAMD51. Upgrade à la v1.1.8: toujours pas de support: 
 			Désactivé les classes AsyncServoWinch. Utiliser celle d'adafruit: https://github.com/adafruit/?q=servo 
 			Mais fonctionne-t-elle sur les cartes non-adafruit?
 		
   Tous les utilitaires (utils) à recompiler.  (ne devrait pas compiler TC_Timer...) 
   
  
***** BUG TROUVE!:
	Lorsqu'on a main + 1 sub + ground actifs, quand on fait plusieurs start/stop avec le 
	sub, à un moment le ground ne reçoit plus rien. Problème de sleep ? Il semble
	que le sleepRQ reste à 1....
	ACTIONS:
		1. changer la config des sub pour mettre SM à 0. Le passer à 1 uniquement 
		   lors de la configuration du Sleep Mode. Cela évitera le besoin d'avoir des 
		   pull-down (documenter).
		   	CODE. A TESTER. OK. 
		2. Instrumenter pour détecter les cas où un send/print/check se passerait sans que le 
		   la XBee soit réveillée et "réparer" et instrumenter autoWake pour détecter un
		   cas où il n'y a PAS de réel wakeUp alors qu'elle dort. 
		    FAIT: pas de détection d'erreur. 
		3.  Détecter le statut de la XBee sur base de ON_SLEEP
		    FAIT: testé avec le test unitaire.  OK
		4.  A ce stade: 
			test avec la sub seule, démarrée APRES le ground: 
				- sans envoyer à la main, et sans délai: 
						on perd des records avec period=70ms. 
						ok avec period 100ms 
						si on allume la main can, qu'on fait un start campaign sur
						la sub (tout OK) puis un stop sur la sub: le ground ne reçoit
						plus rien de la sub (qui envoie apparemment encore). 
						L'APPARITION D'UN COORDINATEUR BLOQUE LES CHOSES.
						si je redémarre la sub (avec power off de la XBee): pas de changement.
						Si je redémarre la sub avec l'autre XBee: idem.
			test en démarrant le coordinateur d'abord, puis le sol, puis les subcans
			    - pas de communication subcan sol.
			    - j'éteins la main et je rallume la sub: OK. 
			      VISIBLEMENT LE COORDINATEUR EMPECHE LA CONNEXION DES END-DEVICES.
			      L'autorisation donnée par le coordinateur dépend de la Security Policy
			      Normalement si EE=0, tout module est autorisé, et c'est le cas.
			      Si NJ=0xFF il n'y a aucune limitation de durée et c'est le cas.  
				  Si JV=0, router+end device fonctionnent sans coord, et c'est le cas. 
				  Les AI (Association indicators) sont tous à Succces. 
				  NC n'est pas nul, il reste de la place dans les child tables.
				  SP et SN étaient mal configurés. Corrigé pour permettre jusqu'à 56 sec
				  de sleep sans que le end-device ne se fasse virer. 
			test en démarrant le coordinateur d'abord, puis le sol, puis les subcans
		    	  Idem: pas de communication sub/ground.
		    test en démarrant coord puis sub puis ground:
		          Idem
		    Sans la main can, aucun problème....
		    Le problème serait-il le nombre de Hops qu'on a limité à 1 ? le message serait-il
		    relayé par la can, et limité à un hop n'irait-il pas plus loin ?? 
		    OUI!!!! C'est ça. Plus de problème avec NH = 2. 
		5. Performances: avec les subs à 100ms et la main à 70 ms, on reçoit:
				- au sol, tous les records de la main, et au minimum 1 rec tous les 200 msec
				  de chaque sub. 
				- dans la main, tous les records des 2 subs. 
		   Si la main can disparait (XBee non alimentée) le sol ne reçoit plus que les
		   subs qui sont ses enfants (impossible à contrôler). Dans mes tests, c'est parfois
		   1, parfois 0. 
		   A TESTER: ce problème disparaît-il si on fait des subs des routers à partir du 
		             start campaign? 
		   OUI! Mais on peut jusqu'à 6 secondes pour passer de router à end-device ou inversément. 
		        Ceci n'est pas un souci: l'éjection se fait 18 secondes après décollage. Adopté. 
				 
		?. (?Inutile) Utiliser une classe pour activation par le constructeur/destructeur. Meilleur
		   design! A test d'abord en UT.
		   		- Plus de methode publique pour le sleep/wake.   
		   		- Plus de wait pour le sleep ou alors dans le constructeur de la classe?
		   		- class XBeeActivator friend de XBeeClient utilisée en interne comme en
		   		  externe. 
		   		     class XBeeActivator {
		   		       public: XBeeActivator(XBeeClient& xbc) {
		   		        		   // même logique, + détection d'erreur (en DBG)
		   		        		   		si pas d'activation nécessaire alors qu'elle dort.
		   		       			}
		   	
NEXT STEPS:
	- Finaliser SecondaryMissionCtrl (A TESTER)
	- RT_Commander: work with Locks, check who provides it. 
	  (créé par le SecondaryMissionController, transmis par le GMiniMainAcquisitionProcess,
	   utilisé par le GMiniCanCommander et le SecondaryMissionController. 
	   Pour supporter les deux: 
	   	 - Changer les noms dans CansatInterface et utilisateurs.
	   	 - Changer le type ServoLatch en Latch dans:
	   	 		- GMiniMainAcquisitionProcess
	   	 		- GMiniSecondaryMissionController::getServoLatch()
	   	 		- GMiniCanCommander
	    Pour implementer le ScrewLatch:
	   	 - Dans GMiniConfig.h ajouter les infos de configuration du ScrewLatch s'il
	   	   en faut. Ajouter une onstante bool  GMiniUseScrewLatch (true pour ScrewLatch
	   	   et false pour ServoLatch. 
 		 - Créer un ServoLatch ou un ScrewLatch dans GMiniSecondaryMissionController::begin()
 		   selon une constant GMiniUseScrewLatch, à ajouter dans GMiniConfig.h
 		 
	- Remplacer la XBee de la RF station par la 8 et en profiter pour écrire la
	  procédure opérationnelle. 
		- Corriger le setA ds CansatConfig
		- Corriger GMiniConfig.h
		- Tout reconfigurer avec l'utilitaire GMini
		- Recharger RF-Transceiver+Main+Subs. 
	- Problème d'irrégularité.
		* Actuellement, on ne traite qu'un seul record entrant par loop(). Le problème
		  d'irrégularité doit donc être lié au traitement d'UN SEUL record entrant, 
		  qui consomme plus que le temps encore dispo avant le cvycle d'acquisition. 
		* Donc si on veut améliorer la régularité, il faut ne pas entamer de réception 
		  moins de tmax avant le cycle d'acquisition (où tmax est le temps de traitement 
		  maximum d'un record entrant). Cela demande:
		  	- de connaitre tmax
		  	- de connaitre au retour de run le temps avant le cycle suivant (valeur de 
		  	  retour de run? facile à faire.)
		* On pourrait aussi accepter l'irrégularité, qui ne pose en fait aucun problème.
		  Dans ce cas, utiliser une tolérant dans la station de base pour le feedback utilisateur.  OK  	
	- Startup procedure and integration tests. 
	- FileBasedParameters? 
TODO: storageManager::getSdFat() peut renvoyer null. Le teste-t-on bien, par exemple
  		dans RT-Commander? 
  		   
BUG MINEUR: lorsqu'on envoie des string parts et que le dernier caractère d'une part est un \n, il est perdu à l'arrivée 
 (cf. diagnostic HW Scanner reçu sur le Transceiver). 


PARAMETERS ADAPTABLES EN DERNIERE MINUTE:
 - Classe FileBasedParameter, codée. UT OK. 
 - Lecture au niveau du processus au moment d'initialiser le secondaryController?  
   (GMiniAcqProcess::initSpecificProject()).  Valeur passée dans  secondary::begin()?
 - Méthode sur secondary: refreshParameters(SD*).
 		FileBaseParameter<uint16_t> x("xxxxx");
 		if (!x.readValue(myUinit16_t)) myUInt16_t=defaultValue (from Config.h).
 - RT_CanCommander : 
      commande standard: SetParameterValue, GetParameterValue +
      	ParameterOperationKO, ParameterVale, ParameterValueSet. CansatInterface.h: OK
      Traitement des commandes à implémenter. 
      Prévoir:
      	- une methode secondaryMissionController::refreshFileBasedParameters() 
      	- une methode RT_CanCommander::forceFileBasedParametersRefresh() qui 
      	  appelle celle du SecondaryMissionController.  
 - Utiliser pour la pression atmospherique et l'altitude sol aussi ?
   Si oui propager le refresh jusqu'au BMP_Client!
 - Documenter dans SW_Architecture. 
      					 
========= NOT DONE ============== 
	
Vérifier carte SD avec version plus récente de Adafruit SAMD. 

MAIN:  EN API, il ne faut pas envoyer le 1, initial, en transparent bien. A DOCUMENTER DANS L'ARCH. 

========= A FAIRE APRES CLOTURE DU PROJET, POUR LES SUIVANTS ==========


Faire une classe de base SecondaryMissionController? OK, test OK.
  * Attention, c'est le CansatAcquisition process qui envoie le record, donc 
    faut gérer la mission secondaire depuis CansatAcquisitionProcess::acquireDataRecord() (entre acquireSecondaryMissionData()
    et l'émission/stockage). OK
     * Avoir une méthode qui renvoie le SecondaryMissionController, comme pour le 
       HW_Scanner?  ou un getSecondaryMissionController qui renvoie un pointeur, et 
       appeler le begin et le run(). OUI. Fait Ok
  * Conséquence: la période d'acquisition contraint la période de gestion (qui doit être un multiple ou identique).
    (identique suffit sans doute: le SecondaryMissionCtrl peut rallentir si utile). OK, doc dans CansatConfig OK
  * Documenter dans la strategie de subclassage (doc?) Ok dans le header de SecondaryMC, TODO dans SW-Arch
  * Il faut annuler des changements dans le main. OK.
  * Main adapté à tester OK  
  * Lien avec le xxxRT_Commander? Il devra avoir accès à GMiniSecondaryMissionCtrl::lock/unlock/neutral.
    C'est le GMiniAcquisitionProcess qui devra fournir un getSecondaryMissionController(). A prévoir en standard? 
    OUI: le RT_CanCommander a un pointer sur l'acqProcess. Par là, il aura accès à tout.  OK 
  * Compléter l'arch software. OK
  
  
  
  * Documenter la réutilisation du RF_Transceiver + RT_Processing, + javascript back-end.   
  * Gérer la "LED multicolore" des ItsyBitsy? 
    
  
  	  
Reprendre l'ownership de torus_rt_processing
Reprendre l'ownership du back-end Javascript.

RF-Transceiver GUI: generaliser? 

Vérifier procédure GoogleEarth et back end. 
				

 
      

