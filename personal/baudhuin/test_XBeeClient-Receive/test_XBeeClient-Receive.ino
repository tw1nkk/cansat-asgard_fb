/**
   Test for the receiving part of XBeeClient.h
*/
#include "IsaTwoXBeeClient.h"
#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG 1

HardwareSerial &RF = Serial1;
IsaTwoXBeeClient xbc(0x0013a200, 0x41827f67);

uint8_t* payload;
uint8_t payloadSize;

void setup() {
  Serial.begin(115200);
  while (!Serial) {
    ;
  }
  digitalWrite(LED_BUILTIN, HIGH);
  DPRINTS(DBG, "Initialising Serials and communications...");
  RF.begin(115200);
  xbc.begin(RF);
  DPRINTS(DBG, "---Beginning of the Tests---");
}

void loop() {
  bool result = xbc.receive(payload, payloadSize);
  if (result) {
    xbc.printFrame(payload, payloadSize);
  } else {

  }
}
