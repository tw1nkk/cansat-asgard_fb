#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_LOCK 1
#define DBG_UNLOCK 1
#include "ServoControl.h"

bool ServoDetach::begin( const uint8_t PWM_Pin) {
  return true;
  Serial.begin(115200);
}

void ServoDetach::unlock() {
  DPRINTS(DBG_UNLOCK, "ServoLatch::unlock, using pin #");
  DPRINTLN(DBG_UNLOCK, thePWM_Pin);
  myServoDetach.attach(thePWM_Pin);
  DPRINTLN(DBG_UNLOCK, "Attaching");
  delay(DelayAfterAttach);
  DPRINTS(DBG_UNLOCK, "Waiting=");
  DPRINTLN(DBG_UNLOCK, DelayAfterAttach);
  myServoDetach.write(UnLockPosition);
  DPRINTS(DBG_UNLOCK, "Writing=");
  DPRINTLN(DBG_UNLOCK, UnLockPosition);
  delay(DelayBeforeDetach);
  DPRINTS(DBG_UNLOCK, "Waiting=");
  DPRINTLN(DBG_UNLOCK, DelayBeforeDetach);
  myServoDetach.detach();
  DPRINTLN(DBG_UNLOCK, "Detaching");
}

void ServoDetach::lock () {
  DPRINTS(DBG_LOCK, "ServoLatch::lock, using pin #");
  DPRINTLN(DBG_LOCK, thePWM_Pin);
  myServoDetach.attach(thePWM_Pin);
  DPRINTLN(DBG_LOCK, "Attaching");
  delay(DelayAfterAttach);
  DPRINTS(DBG_LOCK, "Waiting=");
  DPRINTLN(DBG_LOCK, DelayAfterAttach);
  myServoDetach.write(LockPosition);
  DPRINTS(DBG_LOCK, "Writing=");
  DPRINTLN(DBG_LOCK, UnLockPosition);
  delay(DelayBeforeDetach);
  DPRINTS(DBG_LOCK, "Waiting=");
  DPRINTLN(DBG_LOCK, DelayBeforeDetach);
  myServoDetach.detach();
  DPRINTLN(DBG_LOCK, "Detaching");
}
