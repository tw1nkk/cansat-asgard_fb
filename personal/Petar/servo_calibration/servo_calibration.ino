/*
   Orange = PWM signal
   Red = VCC (power input should be 4.8V (~5V) but the Arduino Feather only outputs 3.3V. Consequences should be tested.
   Brown = GND
*/
#include <Servo.h>

Servo servo;
const uint8_t PWM_Pin = 9;
const uint16_t minPulseWidth = 1000;
const uint16_t maxPulseWidth = 2000;
const uint16_t maxAngle = 1890;

void waitForUser(char msg[] = "Press any key to continue") {
  while (Serial.available() > 0) {
    Serial.read();
  }
  Serial.println(msg);
  Serial.flush();
  while (Serial.available() == 0) {
    delay(300);
  }
  Serial.read();
}

void checkIfUserChanges() {
  long recv;
  while(Serial.available() > 0) {
    recv = Serial.parseInt();
    servo.writeMicroseconds(servo.readMicroseconds() + recv);
    Serial.print("Currently at: "); Serial.println(servo.readMicroseconds());
  }
}

void testMinMaxMid() {
  waitForUser("Press any key to set angle to 90");
  servo.write(90);
  waitForUser("Press any key to set angle to 0");
  servo.write(0);
  waitForUser("Press any key to set angle to 180");
  servo.write(180);
  waitForUser("Press any key to set angle to 0");
  servo.write(0);
  waitForUser("Press any key to set angle to 180");
  servo.write(180);
  waitForUser("Press any key to set angle to 90");
  servo.write(90);
}

void testBigChanges() {
  waitForUser("Press any key to test big changes to angle (angle will change by 80-90 degrees)");
  for (int i = 0; i <= 9; i++) {
    servo.write(i * 10);
    Serial.print("Current angle is "); Serial.println(servo.read());
    delay(1000);
    servo.write((i + 9) * 10);
    Serial.print("Current angle is "); Serial.println(servo.read());
    delay(1000);
  }
}

void testSmallChanges() {
  waitForUser("Press any key to test small changes to angle (angle will change by 10 degrees)");
  for (int i = 18; i >= 0; i--) {
    servo.write(i * 10);
    Serial.print("Current angle is "); Serial.println(servo.read());
    delay(1000);
  }
  for (int i = 0; i <= 18; i++) {
    servo.write(i * 10);
    Serial.print("Current angle is "); Serial.println(servo.read());
    delay(1000);
  }
}

void setup() {
  Serial.begin(115200);
  while (!Serial) {}
  servo.attach(PWM_Pin, minPulseWidth, maxPulseWidth);
  servo.write(0);
}

void loop() {
  checkIfUserChanges();
}
