#include "GPY.h"
#include "IsatisConfig.h"
void setup() {
  // put your setup code here, to run once:
  Serial.begin(115200);
  pinMode(Fan_DigitalPin, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  GPY::setFanOn(true);
  digitalWrite(LED_BUILTIN, HIGH);
  Serial.println(F("FAN On"));
  delay(5000);
  GPY::setFanOn(false);
  digitalWrite(LED_BUILTIN, LOW);
  Serial.println(F("FAN Off"));
  delay (5000);
}
